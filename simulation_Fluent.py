# -*- coding: utf-8 -*-
"""
Created on Mon Feb 19 15:10:42 2018

This scipt is to perform the modelica simulations based on LHS samples and to 
save all the necessary results automatically

@author: Oliver Wang
"""
import os
import numpy as np
import shutil
import winsound
import time
import timeit
import pyDOE
import pandas as pd
import glob
import subprocess
import sys
############################################################
#Main script
if __name__ == "__main__":  
    # Initialize and optimization time
    OTime = 0
    TTime = 0
    Start = timeit.default_timer()
    
    repoPath=os.getcwd()
    CFDPath = repoPath+"/CFD simulation"
    
    ScenarioNumber = 75
    
    #Options for start over or not
    startOver = raw_input('Are you sure you want to start over? Answer with y/n____________')
    if startOver == 'y':
        ## Create result folder
        if os.path.isdir(repoPath + "/Results"):
                shutil.rmtree(repoPath + "/Results", ignore_errors=True)
        os.mkdir(repoPath+'/Results')
        
        ## LHS generate samples

        LHSArray = pyDOE.lhs(7,samples=ScenarioNumber,criterion='maximin')
        LHSArrayDF = pd.DataFrame(LHSArray,columns = ['InternalLoad','CeilingTemp','FloorTemp','SupplyAirTemp','WallTemp','PLC','riseTime'])
        LHSArrayDF.to_csv(os.path.join(repoPath,'Results','LHSArray.csv'))
           
        ## Loop in samples   
        for i in range(ScenarioNumber):
            ## Delete old result files
            filelist = ['MixConvectionWithBoxmsh.cas','MixConvectionWithBoxmsh.dat','InitialSteadyState.jpg','newfinal.jou']
            for fil in filelist:
                if os.path.exists(os.path.join(CFDPath,fil)):
                    os.remove(os.path.join(CFDPath,fil))
            os.chdir(CFDPath)
            outputlist = glob.glob('*.out')
            for fil in outputlist:
                if os.path.exists(os.path.join(CFDPath,fil)):
                    os.remove(os.path.join(CFDPath,fil))
            loglist = glob.glob('*.log')
            for fil in loglist:
                if os.path.exists(os.path.join(CFDPath,fil)):
                    os.remove(os.path.join(CFDPath,fil))
            batlist = glob.glob('*.bat')
            for fil in batlist:
                if os.path.exists(os.path.join(CFDPath,fil)):
                    os.remove(os.path.join(CFDPath,fil))      
            ## Modify the txt input files according to the sample values
            f = open('final.jou','r')
            lines = f.readlines()
            f.close()
            
            #Substitute corresponding values
            lines[34] = '(cx-gui-do cx-set-real-entry-list \"Wall*Frame3*Frame2(Thermal)*Frame1*Frame1(Thermal Conditions)*Table3*Table1*Table2*RealEntry2(Heat Flux)\" \'( %f))\n'%(LHSArray[i,0]*(40-16)+16)
            lines[41] = '(cx-gui-do cx-set-real-entry-list \"Wall*Frame3*Frame2(Thermal)*Frame1*Frame1(Thermal Conditions)*Table3*Table1*Table1*RealEntry2(Temperature)\" \'( %f))\n'%(LHSArray[i,1]*(30-20)+293.15)
            lines[48] = '(cx-gui-do cx-set-real-entry-list \"Wall*Frame3*Frame2(Thermal)*Frame1*Frame1(Thermal Conditions)*Table3*Table1*Table1*RealEntry2(Temperature)\" \'( %f))\n'%(LHSArray[i,2]*(25-20)+293.15)
            lines[54] = '(cx-gui-do cx-set-real-entry-list \"Mass-Flow Inlet*Frame3*Frame2(Thermal)*Table1*Table1*RealEntry2(Total Temperature)\" \'( %f))\n'%(LHSArray[i,3]*(16-14)+287.15)
            lines[64] = '(cx-gui-do cx-set-real-entry-list \"Wall*Frame3*Frame2(Thermal)*Frame1*Frame1(Thermal Conditions)*Table3*Table1*Table1*RealEntry2(Temperature)\" \'( %f))\n'%(LHSArray[i,4]*(30-20)+293.15)
            
            f1 = open('newfinal.jou','w+')
            f1.writelines(lines)
            f1.close()
            
            ## Run fluent
            subprocess.call('fluent 3ddp -t8 -i newfinal.jou')
            
            # Wait until fluent finishes the calculation
            while not os.path.exists('MixConvectionWithBoxmsh.dat'):
                time.sleep(300)
            
            # Copy the result from building Path to the result path
            for fil in filelist+outputlist:
                if os.path.exists(fil):
                    if os.path.exists(repoPath+'/Results/Scenario-'+str(i+1)):
                        shutil.copy(fil,repoPath+'/Results/Scenario-'+str(i+1))
                    else:
                        os.makedirs(repoPath+'/Results/Scenario-'+str(i+1))
                        shutil.copy(fil,repoPath+'/Results/Scenario-'+str(i+1))
    elif startOver == 'n':
        Finished = input('How many cases have been finished? Answer with Integers_____________')
        if type(Finished) != int:
            sys.exit('I said answer with integer!!!')
        resultsPath = repoPath+'/Results'
        # Read the existing uncertain parameter matrix
        LHSArray = pd.read_csv(resultsPath+'/LHSArray.csv').iloc[:,1:].values
        for i in np.arange(Finished,Finished+ScenarioNumber,1):
            ## Delete old result files
            filelist = ['MixConvectionWithBoxmsh.cas','MixConvectionWithBoxmsh.dat','InitialSteadyState.jpg','newfinal.jou']
            for fil in filelist:
                if os.path.exists(os.path.join(CFDPath,fil)):
                    os.remove(os.path.join(CFDPath,fil))
            os.chdir(CFDPath)
            outputlist = glob.glob('*.out')
            for fil in outputlist:
                if os.path.exists(os.path.join(CFDPath,fil)):
                    os.remove(os.path.join(CFDPath,fil))
            loglist = glob.glob('*.log')
            for fil in loglist:
                if os.path.exists(os.path.join(CFDPath,fil)):
                    os.remove(os.path.join(CFDPath,fil))
            batlist = glob.glob('*.bat')
            for fil in batlist:
                if os.path.exists(os.path.join(CFDPath,fil)):
                    os.remove(os.path.join(CFDPath,fil))      
            ## Modify the txt input files according to the sample values
            f = open('final.jou','r')
            lines = f.readlines()
            f.close()
            
            #Substitute corresponding values
            lines[34] = '(cx-gui-do cx-set-real-entry-list \"Wall*Frame3*Frame2(Thermal)*Frame1*Frame1(Thermal Conditions)*Table3*Table1*Table2*RealEntry2(Heat Flux)\" \'( %f))\n'%(LHSArray[i,0]*(40-16)+16)
            lines[41] = '(cx-gui-do cx-set-real-entry-list \"Wall*Frame3*Frame2(Thermal)*Frame1*Frame1(Thermal Conditions)*Table3*Table1*Table1*RealEntry2(Temperature)\" \'( %f))\n'%(LHSArray[i,1]*(30-20)+293.15)
            lines[48] = '(cx-gui-do cx-set-real-entry-list \"Wall*Frame3*Frame2(Thermal)*Frame1*Frame1(Thermal Conditions)*Table3*Table1*Table1*RealEntry2(Temperature)\" \'( %f))\n'%(LHSArray[i,2]*(25-20)+293.15)
            lines[54] = '(cx-gui-do cx-set-real-entry-list \"Mass-Flow Inlet*Frame3*Frame2(Thermal)*Table1*Table1*RealEntry2(Total Temperature)\" \'( %f))\n'%(LHSArray[i,3]*(16-14)+287.15)
            lines[64] = '(cx-gui-do cx-set-real-entry-list \"Wall*Frame3*Frame2(Thermal)*Frame1*Frame1(Thermal Conditions)*Table3*Table1*Table1*RealEntry2(Temperature)\" \'( %f))\n'%(LHSArray[i,4]*(30-20)+293.15)
            
            f1 = open('newfinal.jou','w+')
            f1.writelines(lines)
            f1.close()
            
            ## Run fluent
            subprocess.call('fluent 3ddp -t8 -i newfinal.jou')
            
            # Wait until fluent finishes the calculation
            while not os.path.exists('MixConvectionWithBoxmsh.dat'):
                time.sleep(300)
            
            # Copy the result from building Path to the result path
            for fil in filelist+outputlist:
                if os.path.exists(fil):
                    if os.path.exists(repoPath+'/Results/Scenario-'+str(i+1)):
                        shutil.copy(fil,repoPath+'/Results/Scenario-'+str(i+1))
                    else:
                        os.makedirs(repoPath+'/Results/Scenario-'+str(i+1))
                        shutil.copy(fil,repoPath+'/Results/Scenario-'+str(i+1))
        