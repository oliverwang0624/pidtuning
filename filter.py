# -*- coding: utf-8 -*-
"""
Created on Tue Apr 03 10:30:43 2018

@author: Administrator
"""

import pandas as pd
ewma = pd.Series.ewm
from buildingspy.io.outputfile import Reader 
import os 
import matplotlib.pyplot as plt
import numpy as np

def modifiedEWMA(Series,delta,alpha=0.15):
    #This is the modified EWMA referenced from Shengwei Wang's paper
    #Series is the raw data series to be filtered, which should be a serie
    #alpha is the smooth factor for exponential weighted moving average method
    #delta is tolerance
    
    rawEWMA = Series.ewm(alpha=alpha).mean()
    VCorrect = np.vectorize(correct)
    correctedEWMA = VCorrect(Series,rawEWMA,delta)
    correctedEWMA = pd.Series(correctedEWMA,name = rawEWMA.name)
    return correctedEWMA

def addUniformDistributedNoise(originDF,accuracy):
    noise = np.random.uniform(-accuracy,accuracy,[np.size(originDF,axis=0),np.size(originDF,axis=1)-1])
    addNoise = originDF.iloc[:,1:np.size(originDF.columns)]+noise
    modifiedDF = pd.concat([originDF['Time'],addNoise],axis=1)
    return modifiedDF

def correct(rawData,rawEWMA,delta):
    if rawEWMA < rawData-3*delta:
        rawEWMA = rawData-3*delta
    elif rawEWMA > rawData+3*delta:
        rawEWMA = rawData+3*delta
    return rawEWMA
        
    
if __name__ == "__main__":
    # Read the raw data with noise
    Resultfile = os.path.join(os.getcwd(),'Buildings','OpenLoopTestwithUncertainty_readExtTXT.mat')
    r = Reader(Resultfile,'dymola')
    #(time,rawTempNoise) = r.values('plusNormallyDistributedError.y[1]')
    (time,rawTemp) = r.values('roo.yCFD[1]')
    timeDF = pd.DataFrame(time,columns=['Time'])
    rawTempDF = pd.DataFrame(rawTemp,columns = ['rawTemp'])
    resultDF = pd.concat([timeDF,rawTempDF],axis=1)
    rawTempNoiseDF = addUniformDistributedNoise(resultDF,0.5)
    
    #resultDF = pd.concat([timeDF,rawTempNoiseDF,rawTempDF],axis=1)
    
    ##Read the standard deviation
    #(time,delta) = r.values('plusNormallyDistributedError.sigma')
    #delta = delta[0]
    
    rawEWMA = modifiedEWMA(rawTempNoiseDF['rawTemp'],0.2,alpha=0.25)
    rawEWMA1 = rawTempNoiseDF['rawTemp'].ewm(alpha=0.25).mean()
    
    plt.plot(resultDF['Time'].values,resultDF['rawTemp'].values,'r',linewidth=1,label = 'rawTemp')
    plt.plot(resultDF['Time'].values, rawTempNoiseDF['rawTemp'].values,'--',color='0.75',linewidth=1,label = 'rawTempNoise')
    plt.plot(resultDF['Time'].values,rawEWMA,'b',linewidth=1,label = 'modifiedEWMA_Temp')
    plt.plot(resultDF['Time'].values,rawEWMA1,'g',linewidth=1,label = 'rawEWMA_Temp')
    plt.legend()
    plt.savefig('FilterResults.svg',format='svg')
    plt.show()
    plt.plot(resultDF['Time'].values,resultDF['rawTemp'].values-rawEWMA1,'k')
    plt.show()
        

    