# -*- coding: utf-8 -*-
"""
Created on Mon Feb 19 15:10:42 2018

This scipt is to perform the modelica simulations based on LHS samples and to 
save all the necessary results automatically

@author: Oliver Wang
"""
import os
#from buildingspy.io.outputfile import Reader
#import numpy as np
#import scipy.io as sio
import shutil
#import winsound
import timeit
import pyDOE
def translate(model,packagePath,outputPath):
    import os
    from buildingspy.simulate.Simulator import Simulator
    
    # Check input files: if before translation, input files exsit, then delete them.
    fileList=[outputPath+'/dymosim.exe',outputPath+'/dsin.txt',outputPath+'/'+model+'.mat',outputPath+'/../Resources/InputFiles/bouTemInput.txt',
              outputPath+'/'+model+'.mat',outputPath+'/../Resources/InputFiles/PLCUncInput.txt',
              outputPath+'/'+model+'.mat',outputPath+'/../Resources/InputFiles/risTimInput.txt']
    for fil in fileList:
        if os.path.exists(fil):
            os.remove(fil)
    ## Translate model to get dymosim.exe and dsin.txt
    s = Simulator(model, "dymola")
    s.setOutputDirectory(outputPath)
    s.setPackagePath(packagePath)
    s.showGUI(show=False)
    s.printModelAndTime()
    # This attribute does not exist in the official buildingpy library
    s.chooseCurWorDir(curWorDir= True)
    s.translate()
    if outputPath != packagePath:
        s._copyResultFiles(packagePath)
    return s


############################################################
#Main script
if __name__ == "__main__":    
    repoPath=os.getcwd()
    buildingsPath = repoPath+"/Buildings"
    
    ## Create result folder
    if os.path.isdir(repoPath + "/Results"):
            shutil.rmtree(repoPath + "/Results", ignore_errors=True)
    os.mkdir(repoPath+'/Results')
    
    # Initialize and optimization time
    OTime = 0
    TTime = 0
    Start = timeit.default_timer()
    
    ## LHS generate samples
    ScenarioNumber = 100
    LHSArray = pyDOE.lhs(5,samples=ScenarioNumber,criterion='maximin')
    
    ## Translate model
    model = 'ModelicaModels.System.OpenLoopTestwithUncertainty_readExtTXT'
    s = translate(model,repoPath+"\ModelicaModels",buildingsPath)
    
    # Simulation settings
    startTime = 0
    endTime = 1500
    solver = 8
    Increment = 5
    tolerance = 0.000001
    f=open(buildingsPath+'/dsin.txt','r') 
    lines=f.readlines()
    f.close()
    # Set user-defined solver: Dassl
    lines[9] = "    %d                   # StartTime    Time at which integration starts\n" %(startTime)
    lines[11]= "    %d                   # StopTime     Time at which integration stops\n" %(endTime)
    lines[12]= "    %d                   # Increment    Communication step size, if > 0\n"%(Increment)
    lines[14]=" %f             # Tolerance    Relative precision of signals for\n" %(tolerance)
    lines[18] = "      %d                    # Algorithm    Integration algorithm as integer (1...28)\n" %(solver)
    f=open(buildingsPath+'/dsin.txt','w')
    for k in range(len(lines)):
        f.writelines(lines[k])
    f.close()
    
    ## Loop in samples
    for i in range(ScenarioNumber):
        ## Delete old result files
        if os.path.exists(buildingsPath+'/OpenLoopTestwithUncertainty_readExtTXT.mat'):
            os.remove(buildingsPath+'/OpenLoopTestwithUncertainty_readExtTXT.mat')
    
        
        ## Modify the txt input files according to the sample values
        # bouTemInput file
        f=open(repoPath+"/Resources/InputFiles/bouTemInput.txt","w+")
        f.write("Wall=%f;\n"%(LHSArray[i,0]*(35-20)+293.15))
        f.write("Floor=%f;\n"%(LHSArray[i,1]*(34-19)+292.15))
        f.write("Ceiling=%f;\n"%(LHSArray[i,2]*(33-18)+291.15))
        f.close()
        
#        # .cfd file to change the temperature of the inside block
#        f=open(buildingsPath+'/Resources/Data/Rooms/FFD/MixedConvectionWithBox16sensors.cfd','r') 
#        lines=f.readlines()
#        f.close()
#        lines[28] = "7 7 1 8 8 10 1 %f\n" %(LHSArray[i,3]*(36.7-28)+28)
#        f=open(buildingsPath+'/Resources/Data/Rooms/FFD/MixedConvectionWithBox16sensors.cfd','w+')
#        for k in range(len(lines)):
#            f.writelines(lines[k])
#        f.close()
        
        # PLCUncInput file
        f=open(repoPath+"/Resources/InputFiles/PLCUncInput.txt","w+")
        f.write("UncFacPLC=%f;\n"%(LHSArray[i,3]*(1.2-0.8)+0.8))
        f.close() 
        
        # risTimInput file
        f=open(repoPath+"/Resources/InputFiles/risTimInput.txt","w+")
        f.write("riseTime=%f;\n"%(LHSArray[i,4]*(120-90)+90))
        f.close()
        
        ## Run modelica model
        os.chdir(buildingsPath)
        os.system(buildingsPath+'\dymosim.exe '+ buildingsPath+'\dsin.txt')
        
        # Copy the result from building Path to the result path
        fileList=['dsres.mat','result.plt','unsteady.plt']
        for fil in fileList:
            if os.path.exists(fil):
                if os.path.exists(repoPath+'/Results/Scenario-'+str(i+1)):
                    shutil.copy(fil,repoPath+'/Results/Scenario-'+str(i+1))
                else:
                    os.makedirs(repoPath+'/Results/Scenario-'+str(i+1)) 
                    shutil.copy(fil,repoPath+'/Results/Scenario-'+str(i+1))
                
        for j in range(endTime):
            if os.path.exists('step'+str(j)+'.plt'):
                if os.path.exists(repoPath+'/Results/Scenario-'+str(i+1)+'/Transient plt Files/'):
                    shutil.copy('step'+str(j)+'.plt',repoPath+'/Results/Scenario-'+str(i+1)+'/Transient plt Files/')
                else:
                    os.makedirs(repoPath+'/Results/Scenario-'+str(i+1)+'/Transient plt Files/')
                    shutil.copy('step'+str(j)+'.plt',repoPath+'/Results/Scenario-'+str(i+1)+'/Transient plt Files/')
               