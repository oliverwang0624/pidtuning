#include "udf.h"
DEFINE_PROFILE(inlet_mf,th,i)
{
	face_t f;
	begin_f_loop(f,th)
	{
		if(CURRENT_TIME <= 500)
			F_PROFILE(f,th,i) = 0.1;
		else if(CURRENT_TIME <=1500 && CURRENT_TIME >500)
			F_PROFILE(f,th,i) = 0.0399672;
	}
	end_f_loop(f,th);
}
