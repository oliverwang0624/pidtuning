import re

TimeStep = 5
SimulationTime = 3000
FilNam = range(0,SimulationTime/TimeStep,1)
STid = range(0,SimulationTime,TimeStep)
Dic = dict(zip(FilNam, STid))

for Fil in FilNam:
    with open('step'+str(Dic[Fil])+'.plt', "r") as MyFil:
        lines=MyFil.readlines()
    with open('step'+str(Dic[Fil])+'.plt', "w") as MyFil:
        for index, line in enumerate(lines):
            if ("ZONE" in line):
                print ('%d\tFind in step%d.plt ' %(Fil,Dic[Fil]))
                lines[index]=line.replace("ZONE", 'ZONE T="Time%d", STRANDID=1, SOLUTIONTIME=%d,' %(Fil, Dic[Fil]))
        MyFil.writelines(lines)
#        
        


