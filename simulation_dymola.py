# -*- coding: utf-8 -*-
"""
Created on Mon Feb 19 15:10:42 2018

This scipt is to perform the modelica simulations based on LHS samples and to 
save all the necessary results automatically

@author: Oliver Wang
"""
import os
#from buildingspy.io.outputfile import Reader
import numpy as np
#import scipy.io as sio
import shutil
import pandas as pd
#import winsound
import timeit
import pyDOE
import sys
def translate(model,packagePath,outputPath):
    import os
    from buildingspy.simulate.Simulator import Simulator
    
    # Check input files: if before translation, input files exsit, then delete them.
    fileList=[outputPath+'/dymosim.exe',outputPath+'/dsin.txt',outputPath+'/dsres.mat',outputPath+'/../Resources/InputFiles/bouTemInput.txt',
              outputPath+'/../Resources/InputFiles/PLCUncInput.txt',
              outputPath+'/../Resources/InputFiles/risTimInput.txt',outputPath+'/../Resources/InputFiles/TransferFunInput.txt']
    for fil in fileList:
        if os.path.exists(fil):
            os.remove(fil)
    ## Translate model to get dymosim.exe and dsin.txt
    s = Simulator(model, "dymola")
    s.setOutputDirectory(outputPath)
    s.setPackagePath(packagePath)
    s.showGUI(show=False)
    s.printModelAndTime()
    # This attribute does not exist in the official buildingpy library
    s.chooseCurWorDir(curWorDir= True)
    s.translate()
    if outputPath != packagePath:
        s._copyResultFiles(packagePath)
    return s


############################################################
#Main script
if __name__ == "__main__":    
    repoPath=os.getcwd()
    resultPath = repoPath+'/Results_fluent'
    buildingsPath = repoPath+"/Buildings"
    

    
    # Initialize and optimization time
    OTime = 0
    TTime = 0
    Start = timeit.default_timer()
    
    ## LHS generate samples
    ScenarioNum = 100
    Finished = 0
    #Options for start over or not
    startOver = raw_input('Are you sure you want to start over? Answer with y/n____________')
    if startOver == 'y':
        ## Create result folder
        if os.path.isdir(repoPath + "/Results_dymola"):
                shutil.rmtree(repoPath + "/Results_dymola", ignore_errors=True)
                os.mkdir(repoPath+'/Results_dymola')
        else:
            os.mkdir(repoPath+'/Results_dymola')
    elif startOver == 'n':
        Finished = input('How many cases have been finished? Answer with Integers_____________')
        if type(Finished) != int:
            sys.exit('I said answer with integer!!!')  
            
    LHSArray = pd.read_csv(resultPath+'/LHSArray.csv').iloc[:,1:].values
    
    ## Translate model
    model = 'ModelicaModels.System.OpenLoopTestwithUncertainty_CFDTransferFunction'
    s = translate(model,repoPath+"\ModelicaModels",buildingsPath)
    
    # Simulation settings
    startTime = 0
    endTime = 2000
    solver = 8
    Increment = 5
    tolerance = 0.000001
    f=open(buildingsPath+'/dsin.txt','r') 
    lines=f.readlines()
    f.close()
    # Set user-defined solver: Dassl
    lines[9] = "    %d                   # StartTime    Time at which integration starts\n" %(startTime)
    lines[11]= "    %d                   # StopTime     Time at which integration stops\n" %(endTime)
    lines[12]= "    %d                   # Increment    Communication step size, if > 0\n"%(Increment)
    lines[14]=" %f             # Tolerance    Relative precision of signals for\n" %(tolerance)
    lines[18] = "      %d                    # Algorithm    Integration algorithm as integer (1...28)\n" %(solver)
    f=open(buildingsPath+'/dsin.txt','w')
    for k in range(len(lines)):
        f.writelines(lines[k])
    f.close()
    
    ## Loop in samples
    for i in np.arange(Finished,Finished+ScenarioNum,1):
        
        ## Delete old result files
        if os.path.exists(buildingsPath+'/dsres.mat'):
            os.remove(buildingsPath+'/dsres.mat')
    
        
        ## Modify the txt input files according to the sample values  
        # PLCUncInput file
        f=open(repoPath+"/Resources/InputFiles/PLCUncInput.txt","w+")
        f.write("UncFacPLC=%f;\n"%(LHSArray[i,5]*(1.4-0.6)+0.6))
        f.close() 
        
        # risTimInput file
        f=open(repoPath+"/Resources/InputFiles/risTimInput.txt","w+")
        f.write("riseTime=%f;\n"%(LHSArray[i,6]*(120-90)+90))
        f.close()
        
        # supTempInput file
        f=open(repoPath+"/Resources/InputFiles/supTempInput.txt","w+")
        f.write("supTemp=%f;\n"%(LHSArray[i,3]*(16-14)+287.15))
        f.close()
        # read transfer function parameters
        parameters = pd.read_csv(os.path.join(resultPath,'Summary','Transfer Function_Scenario-'+str(i+1)+'.csv')).iloc[:,1:].values
        
        for j in range(1):             #loop in 16 sensors
            f=open(repoPath+"/Resources/InputFiles/TransferFunInput.txt","w+")
            f.write('K=%f;\n'%(parameters[0,j]))
            f.write('T1=%f;\n'%(parameters[1,j]))
            f.write('T2=%f;\n'%(parameters[2,j]))
            f.write('InitialValue=%f;\n'%(parameters[3,j]))
            f.close()
            
            ## Run modelica model
            os.chdir(buildingsPath)
            os.system(buildingsPath+'\dymosim.exe '+ buildingsPath+'\dsin.txt')
        
            # Copy the result from building Path to the result path
            fileList=['dsres.mat']
            for fil in fileList:
                if os.path.exists(fil):
                    if os.path.exists(repoPath+'/Results_dymola/Scenario-'+str(i+1)):
                        shutil.copy(fil,repoPath+'/Results_dymola/Scenario-'+str(i+1))
                        os.chdir(repoPath+'/Results_dymola/Scenario-'+str(i+1))
                        os.rename(fil,'dsres_'+str(j)+'.mat')
                    else:
                        os.makedirs(repoPath+'/Results_dymola/Scenario-'+str(i+1)) 
                        shutil.copy(fil,repoPath+'/Results_dymola/Scenario-'+str(i+1))
                        os.chdir(repoPath+'/Results_dymola/Scenario-'+str(i+1))
                        os.rename(fil,'dsres_'+str(j)+'.mat')
               