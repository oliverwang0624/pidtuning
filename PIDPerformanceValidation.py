# -*- coding: utf-8 -*-
"""
Created on Mon Feb 19 15:10:42 2018

This scipt is to perform the modelica simulations based on LHS samples and to 
save all the necessary results automatically

@author: Oliver Wang
"""
import os
from buildingspy.io.outputfile import Reader
import numpy as np
import scipy.io as sio
import shutil
import pandas as pd
#import winsound
import timeit
import matplotlib.pyplot as plt


def translate(model,packagePath,outputPath):
    import os
    from buildingspy.simulate.Simulator import Simulator
    
    # Check input files: if before translation, input files exsit, then delete them.
    fileList=[outputPath+'/dymosim.exe',outputPath+'/dsin.txt',outputPath+'/dsres.mat',
              outputPath+'/../Resources/InputFiles/bouTemInput.txt',
              outputPath+'/../Resources/InputFiles/PLCUncInput.txt',
              outputPath+'/../Resources/InputFiles/risTimInput.txt',
              outputPath+'/../Resources/InputFiles/TransferFunInput_validation.txt',
              outputPath+'/../Resources/InputFiles/PIDInput.txt',
              outputPath+'/../Resources/InputFiles/allTransFun.mat',
              outputPath+'/../Resources/InputFiles/TransferFunInput_validationAve.txt']
    for fil in fileList:
        if os.path.exists(fil):
            os.remove(fil)
    ## Translate model to get dymosim.exe and dsin.txt
    s = Simulator(model, "dymola")
    s.setOutputDirectory(outputPath)
    s.setPackagePath(packagePath)
    s.showGUI(show=False)
    s.printModelAndTime()
    # This attribute does not exist in the official buildingpy library
    s.chooseCurWorDir(curWorDir= True)
    s.translate()
    if outputPath != packagePath:
        s._copyResultFiles(packagePath)
    return s


############################################################
#Main script
if __name__ == "__main__":    
    repoPath=os.getcwd()
    resultPath = repoPath+'/Performance Validation'
    buildingsPath = repoPath+"/Buildings"
    
    ScenarioNum = 100
    SensorNum= 16
    
    #Options for start over or not
    startOver = raw_input('Are you sure you want to re-run the simulations? Answer with y/n____________')
    if startOver == 'y':
        #Purge old results
        if os.path.exists(resultPath):
            shutil.rmtree(resultPath)
            os.mkdir(resultPath)
        else:
            os.mkdir(resultPath)
        # Initialize and optimization time
        OTime = 0
        TTime = 0
        Start = timeit.default_timer()

        ## Translate model
        model = 'ModelicaModels.System.PIDPerformanceValidation'
        s = translate(model,repoPath+"\ModelicaModels",buildingsPath)
        ##Rename the translated models and input file respectively for PID and PI control
        os.chdir(buildingsPath)
        os.rename('dymosim.exe','dymosimPID.exe')
        os.rename('dsin.txt','dsinPID.txt')
        
        ## Translate model
        model = 'ModelicaModels.System.PIPerformanceValidation'
        s = translate(model,repoPath+"\ModelicaModels",buildingsPath)
        ##Rename the translated models and input file respectively for PID and PI control
        os.chdir(buildingsPath)
        os.rename('dymosim.exe','dymosimPI.exe')
        os.rename('dsin.txt','dsinPI.txt')
        
        # Simulation settings
        startTime = 0
        endTime = 3000
        solver = 4
        Increment = 1
        tolerance = 0.000001
        for fil in ['dsinPID.txt','dsinPI.txt']:
            f=open(buildingsPath+'/'+fil,'r') 
            lines=f.readlines()
            f.close()
            # Set user-defined solver: Dassl
            lines[9] = "    %d                   # StartTime    Time at which integration starts\n" %(startTime)
            lines[11]= "    %d                   # StopTime     Time at which integration stops\n" %(endTime)
            lines[12]= "    %d                   # Increment    Communication step size, if > 0\n"%(Increment)
            lines[14]= "    %f             # Tolerance    Relative precision of signals for\n" %(tolerance)
            lines[18] = "      %d                    # Algorithm    Integration algorithm as integer (1...28)\n" %(solver)
            f=open(buildingsPath+'/'+fil,'w')
            for k in range(len(lines)):
                f.writelines(lines[k])
            f.close()
         
        PID = pd.read_csv(os.path.join(repoPath,'PID_final.csv')).iloc[:,1:].values
        
        ## Loop in samples
        for i in np.arange(0,ScenarioNum,1):
            LocAveTemp = []
            IdealAveTemp = []
            LocTemp = []
            Setpoint = []
        #loop in 16 sensors
            for j in np.arange(0,SensorNum,1):   
                f=open(repoPath+"/Resources/InputFiles/PIDInput.txt","w+")
                f.write('Kp=%f;\n'%(PID[0,j]))
                f.write('Ti=%f;\n'%(PID[1,j]))
                f.write('Td=%f;\n'%(PID[2,j]))
                f.close()
                
                ## Delete old result files
                if os.path.exists(buildingsPath+'/dsres.mat'):
                    os.remove(buildingsPath+'/dsres.mat')
            
                # read transfer function parameters
                parameters = pd.read_csv(os.path.join(repoPath,'Results_dymola','Summary','Transfer Function_Scenario-'+str(i+1)+'.csv')).iloc[:,1:].values
                
                sio.savemat(repoPath+'\Resources\InputFiles\AllTransferFunctions.mat',{'allTransFuns':parameters})
                
                #Local tempearatures
                f=open(repoPath+"/Resources/InputFiles/TransferFunInput_validation.txt","w+")
                f.write('K=%f;\n'%(parameters[0,j]))
                f.write('T1=%f;\n'%(parameters[1,j]))
                f.write('T2=%f;\n'%(parameters[2,j]))
                f.write('D=%f;\n'%(parameters[3,j]))
                f.write('IniVal=%f;\n'%(parameters[4,j]))
                f.close()
                
                #Ideal average temperature
                f=open(repoPath+"/Resources/InputFiles/TransferFunInput_validationAve.txt","w+")
                f.write('KAve=%f;\n'%(parameters[0,16]))
                f.write('T1Ave=%f;\n'%(parameters[1,16]))
                f.write('T2Ave=%f;\n'%(parameters[2,16]))
                f.write('DAve=%f;\n'%(parameters[3,16]))
                f.write('IniValAve=%f;\n'%(parameters[4,16]))
                f.close()
                
                ## Run modelica model
                os.chdir(buildingsPath)
                if PID[2,j] == 0:
                    os.system(buildingsPath+'\dymosimPI.exe '+ buildingsPath+'\dsinPI.txt')
                else:
                    os.system(buildingsPath+'\dymosimPID.exe '+ buildingsPath+'\dsinPID.txt')    
            
                #Read temperature data 
                ResultFile = os.path.join(buildingsPath,"dsres.mat")
                r = Reader(ResultFile,"dymola")
                
                (time,LocAvetemp) = r.values('transferFuns.y')
                (time,IdealAvetemp) = r.values('FinalValue1.y')
                (time,Loctemp) = r.values('FinalValue.y')
                (time,setpoint) = r.values('Setpoint.y')
                
                LocAvetemp = LocAvetemp.reshape(np.size(LocAvetemp),1)
                IdealAvetemp = IdealAvetemp.reshape(np.size(IdealAvetemp),1)
                Loctemp = Loctemp.reshape(np.size(Loctemp),1)
                setpoint = setpoint.reshape(np.size(setpoint),1)
                
                if j == 0:
                    LocAveTemp.append(LocAvetemp)
                    LocAveTemp = np.asarray(LocAveTemp)
                    LocAveTemp = LocAveTemp.reshape((np.size(LocAveTemp)),1)
                    
                    IdealAveTemp.append(IdealAvetemp)
                    IdealAveTemp = np.asarray(IdealAveTemp)
                    IdealAveTemp = IdealAveTemp.reshape((np.size(IdealAveTemp)),1)
                    
                    LocTemp.append(Loctemp)
                    LocTemp = np.asarray(LocTemp)
                    LocTemp = LocTemp.reshape((np.size(LocTemp)),1)
                    
                    Setpoint.append(setpoint)
                    Setpoint = np.asarray(Setpoint)
                    Setpoint = Setpoint.reshape((np.size(Setpoint)),1)   
                    
                else:
                    LocAveTemp = np.append(LocAveTemp,LocAvetemp,1)
                    IdealAveTemp = np.append(IdealAveTemp,IdealAvetemp,1)
                    LocTemp = np.append(LocTemp,Loctemp,1)
                    Setpoint = np.append(Setpoint,setpoint,1)
                

            #Concat all the data into one dataframe
            timeDF = pd.DataFrame(time,columns=['Time'])
            LocAveTempDF = pd.DataFrame(LocAveTemp,columns=['Sensor1','Sensor2','Sensor3','Sensor4',
                                                  'Sensor5','Sensor6','Sensor7','Sensor8',
                                                  'Sensor9','Sensor10','Sensor11','Sensor12',
                                                  'Sensor13','Sensor14','Sensor15','Sensor16','AveTemp'])
            IdealAveTempDF = pd.DataFrame(IdealAveTemp,columns=['Sensor1','Sensor2','Sensor3','Sensor4',
                                                  'Sensor5','Sensor6','Sensor7','Sensor8',
                                                  'Sensor9','Sensor10','Sensor11','Sensor12',
                                                  'Sensor13','Sensor14','Sensor15','Sensor16','AveTemp'])
            LocTempDF = pd.DataFrame(LocTemp,columns=['LocTemp1','LocTemp2','LocTemp3','LocTemp4',
                                                  'LocTemp5','LocTemp6','LocTemp7','LocTemp8',
                                                  'LocTemp9','LocTemp10','LocTemp11','LocTemp12',
                                                  'LocTemp13','LocTemp14','LocTemp15','LocTemp16','AveTemp'])
            SetpointDF = pd.DataFrame(Setpoint,columns=['Setpoint1','Setpoint2','Setpoint3','Setpoint4',
                                                  'Setpoint5','Setpoint6','Setpoint7','Setpoint8',
                                                  'Setpoint9','Setpoint10','Setpoint11','Setpoint12',
                                                  'Setpoint13','Setpoint14','Setpoint15','Setpoint16','AveTemp'])
            
            Data1 = pd.concat([timeDF,SetpointDF,LocAveTempDF],axis=1)
            UniqueData1 = Data1.drop_duplicates('Time')
            UniqueData1.index = range(np.size(UniqueData1,axis=0))
            UniqueData1.to_csv(resultPath+'/LocAveTemp_Scenario_'+str(i+1)+'.csv')
            
            Data2 = pd.concat([timeDF,SetpointDF,IdealAveTempDF],axis=1)
            UniqueData2 = Data2.drop_duplicates('Time')
            UniqueData2.index = range(np.size(UniqueData2,axis=0))
            UniqueData2.to_csv(resultPath+'/IdealAveTemp_Scenario_'+str(i+1)+'.csv')
                        
            Data3 = pd.concat([timeDF,SetpointDF,LocTempDF],axis=1)
            UniqueData3 = Data3.drop_duplicates('Time')
            UniqueData3.index = range(np.size(UniqueData3,axis=0))
            UniqueData3.to_csv(resultPath+'/LocTemp_Scenario_'+str(i+1)+'.csv')
#                     
#                
##########################################################################################   
    elif startOver == 'n':       
        # Plot the results
        
        #Ideal Average Temperature
        fig, ax = plt.subplots(3,4,figsize=(16,12))
        fig.text(0.09, 0.5, 'Temperature/$^\circ$C', ha='center', va='center', rotation='vertical',fontsize=15)
        fig.text(0.5, 0.07, 'Time/s', ha='center', va='center',fontsize=15)
        
        # loop in sensors
        for i in np.arange(0,SensorNum,1):
            if i < 4:
                k = i
            elif i >= 4 and i < 8:
                continue
            elif i == 8:
                k = 11
            elif i == 9:
                k = 10
            elif i == 10:
                k = 9
            elif i == 11:
                k = 8
            elif i >= 12:
                k = i-8
            ax1 = ax[np.floor_divide(k,4),k-4*np.floor_divide(k,4)]
            
            #loop in scenarios 
            for j in np.arange(0,ScenarioNum,1):
                plotdata = pd.read_csv(os.path.join(resultPath,
                            'IdealAveTemp_Scenario_'+str(j+1)+'.csv')).iloc[:,1:]

                if j == ScenarioNum-1:
                    ax1.plot(plotdata[(plotdata.Time>=1000)]["Time"].values-1000,plotdata[(plotdata.Time>=1000)].iloc[:,i+18].values-273.15,color='0.75', label = "AveTemp")
                    ax1.plot(plotdata[(plotdata.Time>=1000)]["Time"].values-1000,plotdata[(plotdata.Time>=1000)].iloc[:,i+1].values-273.15,'r',label = "Setpoint")
                else:
                    ax1.plot(plotdata[(plotdata.Time>=1000)]["Time"].values-1000,plotdata[(plotdata.Time>=1000)].iloc[:,i+18].values-273.15,color='0.75')
            print i+1
            
            ax1.set_xlabel('Location #'+str(k+1),fontsize = 12)
            ax1.legend(fontsize = 12,loc=1)
            ax1.grid(color='0.75',linestyle='--',linewidth=0.3)            
         
        fig.savefig(repoPath+'/Validation_IdealAveTemp.svg',format='svg',dpi=1000)         
#        
##        ####Local Average temperature
#        fig, ax = plt.subplots(4,4,figsize=(16,16))
#        fig.text(0.09, 0.5, 'Temperature/$^\circ$C', ha='center', va='center', rotation='vertical',fontsize=15)
#        fig.text(0.5, 0.09, 'Time/s', ha='center', va='center',fontsize=15)
#        
#        # loop in sensors
#        for i in np.arange(0,SensorNum,1):
#            
#            ax1 = ax[np.floor_divide(i,4),i-4*np.floor_divide(i,4)]
#            
#            #loop in scenarios 
#            for j in np.arange(0,ScenarioNum,1):
#                plotdata = pd.read_csv(os.path.join(resultPath,
#                            'LocAveTemp_Scenario_'+str(j+1)+'.csv')).iloc[:,1:]
#
#                if j == ScenarioNum-1:
#                    ax1.plot(plotdata[(plotdata.Time>=1000)]["Time"].values-1000,plotdata[(plotdata.Time>=1000)].iloc[:,i+18].values-273.15,color='0.75', label = "LocAveTemp")
#                    ax1.plot(plotdata[(plotdata.Time>=1000)]["Time"].values-1000,plotdata[(plotdata.Time>=1000)].iloc[:,i+1].values-273.15,'r',label = "Setpoint")
#                else:
#                    ax1.plot(plotdata[(plotdata.Time>=1000)]["Time"].values-1000,plotdata[(plotdata.Time>=1000)].iloc[:,i+18].values-273.15,color='0.75')
#            print i+1
#            
#            ax1.set_xlabel('Sensor #'+str(i+1),fontsize = 12)
#            ax1.legend(fontsize = 12,loc=1)
#            ax1.grid(color='0.75',linestyle='--',linewidth=0.3)            
#         
#        fig.savefig(repoPath+'/Validation_LocAveTemp.svg',format='svg',dpi=1000)        

        
        #Local Temperatures
        fig, ax = plt.subplots(3,4,figsize=(16,12))
        fig.text(0.09, 0.5, 'Temperature/$^\circ$C', ha='center', va='center', rotation='vertical',fontsize=15)
        fig.text(0.5, 0.07, 'Time/s', ha='center', va='center',fontsize=15)
        
        # loop in sensors
        for i in np.arange(0,SensorNum,1):
            if i < 4:
                k = i
            elif i >= 4 and i < 8:
                continue
            elif i >= 12:
                k = i-8
            elif i == 8:
                k = 11
            elif i == 9:
                k = 10
            elif i == 10:
                k = 9
            elif i == 11:
                k = 8
            ax1 = ax[np.floor_divide(k,4),k-4*np.floor_divide(k,4)]
            
            #loop in scenarios 
            for j in np.arange(0,ScenarioNum,1):
                plotdata = pd.read_csv(os.path.join(resultPath,
                            'LocTemp_Scenario_'+str(j+1)+'.csv')).iloc[:,1:]

                if j == ScenarioNum-1:
                    ax1.plot(plotdata[(plotdata.Time>=1000)]["Time"].values-1000,plotdata[(plotdata.Time>=1000)].iloc[:,i+18].values-273.15,color='0.75', label = "LocTemp"+str(k+1))
                    ax1.plot(plotdata[(plotdata.Time>=1000)]["Time"].values-1000,plotdata[(plotdata.Time>=1000)].iloc[:,i+1].values-273.15,'r',label = "Setpoint")
                else:
                    ax1.plot(plotdata[(plotdata.Time>=1000)]["Time"].values-1000,plotdata[(plotdata.Time>=1000)].iloc[:,i+18].values-273.15,color='0.75')
            print i+1
            
            ax1.set_xlabel('Location #'+str(k+1),fontsize = 12)
            ax1.legend(fontsize = 12,loc=1)
            ax1.grid(color='0.75',linestyle='--',linewidth=0.3)            
         
        fig.savefig(repoPath+'/Validation_LocTemp.svg',format='svg',dpi=1000)  
            
            
            
            
            
            
            
            
            
            
            
            
            
            