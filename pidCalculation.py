# -*- coding: utf-8 -*-
"""
Created on Thu Apr 26 10:08:59 2018

@author: Administrator
"""
import numpy as np
import sys
import os

import pandas as pd
import shutil


import matplotlib.pyplot as plt
import timeit
import winsound


def pidCalculation(k,t1,t2,d):
    
    if d <= 2:
        d = d + 0.5*t2
        t1 = t1 + 0.5*t2
        Kp = 0.5*t1/(k*d)
        Ti = np.min([t1,8*d])
        Td = 0

    else:
        if t1 <= 8*d:
            Kp = 0.5*(t1+t2)/(k*d)
            Ti = t1+t2
            Td = t2/(1+t2/t1)
        else:
            Kp = 0.5*t1/(k*d)*(1+t2/(8*d))
            Ti = 8*d+t2
            Td = t2/(1+t2/(8*d))
    return Kp, Ti, Td



if __name__ == "__main__":
#Calculate all the pid parameters for every transfer functions
    repoPath = os.getcwd()
    PackagePath = repoPath+'/ModelicaModels'
    ResultPath = repoPath+'/Results_dymola'
    
    ScenarioNum = 100
    Finished = 0

    
    for i in np.arange(Finished,Finished+ScenarioNum,1):
        # Create a dataframe to restore the PID controller's parameters
        Kp = np.zeros([1,17])
        Ti = np.zeros([1,17])
        Td = np.zeros([1,17])
        Kp_DF = pd.DataFrame(Kp,columns=['LocTemp1','LocTemp2','LocTemp3','LocTemp4',
                                                      'LocTemp5','LocTemp6','LocTemp7','LocTemp8',
                                                      'LocTemp9','LocTemp10','LocTemp11','LocTemp12',
                                                      'LocTemp13','LocTemp14','LocTemp15','LocTemp16','AveTemp'])
        Ti_DF = pd.DataFrame(Ti,columns=list(Kp_DF))
        Td_DF = pd.DataFrame(Td,columns=list(Kp_DF))
        PIDparameters_DF = pd.concat([Kp_DF,Ti_DF,Td_DF],axis=0)
        PIDparameters_DF.index = ['kp','Ti','Td']
        
        # Read existing transfer functions 
        TF = pd.read_csv(os.path.join(ResultPath,'Summary','Transfer Function_Scenario-'+str(i+1)+'.csv')).iloc[:,1:]
        for j in range (17):                # loop in sensors
            k = TF.iloc[0,j]
            t1 = TF.iloc[1,j]
            t2 = TF.iloc[2,j]
            d = TF.iloc[3,j]
            [kp,ti,td]=pidCalculation(k,t1,t2,d)
            PIDparameters_DF.iloc[:,j] = [kp,ti,td]
        PIDparameters_DF.to_csv(ResultPath+'/Summary/PID_Scenario-'+str(i+1)+'.csv')
        
##Calculate the pid parameters for the most probable transfer function only
#            
#    repoPath = os.getcwd()
#    TF = pd.read_csv(os.path.join(repoPath,'TF_final.csv')).iloc[:,1:]
#    Kp = np.zeros([1,16])
#    Ti = np.zeros([1,16])
#    Td = np.zeros([1,16])
#    Kp_DF = pd.DataFrame(Kp,columns=['LocTemp1','LocTemp2','LocTemp3','LocTemp4',
#                                                  'LocTemp5','LocTemp6','LocTemp7','LocTemp8',
#                                                  'LocTemp9','LocTemp10','LocTemp11','LocTemp12',
#                                                  'LocTemp13','LocTemp14','LocTemp15','LocTemp16'])
#    Ti_DF = pd.DataFrame(Ti,columns=list(Kp_DF))
#    Td_DF = pd.DataFrame(Td,columns=list(Kp_DF))
#    PIDparameters_DF = pd.concat([Kp_DF,Ti_DF,Td_DF],axis=0)
#    PIDparameters_DF.index = ['kp','Ti','Td']       
#            
#    for j in range (16):                # loop in sensors
#        k = TF.iloc[0,j]
#        t1 = TF.iloc[1,j]
#        t2 = TF.iloc[2,j]
#        d = TF.iloc[3,j]
#        [kp,ti,td]=pidCalculation(k,t1,t2,d)
#        PIDparameters_DF.iloc[:,j] = [kp,ti,td]            
#            
#    PIDparameters_DF.to_csv(repoPath+'/PID_final_TFfirst.csv')         
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            