# -*- coding: utf-8 -*-
"""
Created on Thu Apr 05 08:59:22 2018

@author: Administrator
"""

import numpy as np
import sys
import os
from buildingspy.io.outputfile import Reader 
import pandas as pd
import shutil
from simulation import translate
from pso import pso
import matplotlib.pyplot as plt

def checkReasonable(DataFrame,start,end,criterion):
    for i in range(17):
        if np.max(DataFrame[(DataFrame['Time']>start)&(DataFrame['Time']<end)].iloc[:,i+1].values)-np.min(DataFrame[(DataFrame['Time']>start)&(DataFrame['Time']<end)].iloc[:,i+1].values)>=criterion:
            return 0
    return 1

ScenarioNum = 100
repoPath = os.getcwd()
ResultPath = os.path.join(repoPath,'Results')

#Purge old results
if os.path.exists(ResultPath+'/Summary'):
    shutil.rmtree(ResultPath+'/Summary')
    os.mkdir(ResultPath+'/Summary')
else:
    os.mkdir(ResultPath+'/Summary')
    
fig1, ax1 = plt.subplots(4,4,figsize=(20,20))
fig2, ax2 = plt.subplots(4,4,figsize=(20,20))
fig3, ax3 = plt.subplots(1,1)
fig4, ax4 = plt.subplots(1,1)
wrongScenNum = []
#Loop in scnearios
for i in range(ScenarioNum):
    #Creat dataframe for this scenario
    #Read time and temperature data
    ResultFile = os.path.join(ResultPath,"Scenario-"+str(i+1),
                              "dsres.mat")
    #Read temperature data without noise
    time = []
    AveTemp = []
    r = Reader(ResultFile,"dymola")
    (time,AveTemp) = r.values('roo.yCFD[1]')
    LocTemp = np.zeros([np.size(time,axis=0),16])
    #Loop in 16 local temperature sensors
    for j in range(16):
        (time,loctemp) = r.values('roo.yCFD['+str(j+3)+']')
        LocTemp[:,j] = loctemp
    
    #Concat all the data into one dataframe
    timeDF = pd.DataFrame(time,columns=['Time'])
    AveTempDF = pd.DataFrame(AveTemp,columns=['AveTemp'])
    LocTempDF = pd.DataFrame(LocTemp,columns=['LocTemp1','LocTemp2','LocTemp3','LocTemp4',
                                              'LocTemp5','LocTemp6','LocTemp7','LocTemp8',
                                              'LocTemp9','LocTemp10','LocTemp11','LocTemp12',
                                              'LocTemp13','LocTemp14','LocTemp15','LocTemp16'])
    Data = pd.concat([timeDF,AveTempDF,LocTempDF],axis=1)
    
    #Read temperature data with noise
    (time,RawAveTemp) = r.values('plusNormallyDistributedError.y[1]')
    RawLocTemp = np.zeros([np.size(time,axis=0),16])
    #Loop in 16 local temperature sensors
    for j in range(16):
        (time,Rawloctemp) = r.values('plusNormallyDistributedError.y['+str(j+3)+']')
        RawLocTemp[:,j] = Rawloctemp
    
    #Concat all the data into one dataframe
    timeDF = pd.DataFrame(time,columns=['Time'])
    RawAveTempDF = pd.DataFrame(RawAveTemp,columns=['RawAveTemp'])
    RawLocTempDF = pd.DataFrame(RawLocTemp,columns=['RawLocTemp1','RawLocTemp2','RawLocTemp3','RawLocTemp4',
                                              'RawLocTemp5','RawLocTemp6','RawLocTemp7','RawLocTemp8',
                                              'RawLocTemp9','RawLocTemp10','RawLocTemp11','RawLocTemp12',
                                              'RawLocTemp13','RawLocTemp14','RawLocTemp15','RawLocTemp16'])
    RawData = pd.concat([timeDF,RawAveTempDF,RawLocTempDF],axis=1)
    
    UniqueData = Data.drop_duplicates('Time')
    UniqueData.to_csv(ResultPath+'/Summary/Dymola Results_Scenario-'+str(i+1)+'.csv')
    UniqueRawData = RawData.drop_duplicates('Time')
    UniqueRawData.to_csv(ResultPath+'/Summary/Dymola Results_Scenario-'+str(i+1)+'.csv')
    
    #Check if the dynamic temperature result is reasonable or not
    if checkReasonable(UniqueData,500,1200,1) == 0:
        wrongScenNum.append(i+1)
        continue

    #Plot temperature with and without noise
    for j in range(16):   # loop in axises
        ax11 = ax1[j//4,j-(j//4)*4]
        ax22 = ax2[j//4,j-(j//4)*4]
        ax11.plot(UniqueData['Time'],UniqueData.iloc[:,j+2],color='0.75',linewidth=0.2,label=UniqueData.iloc[:,j+2].name)
        ax22.plot(UniqueRawData['Time'],UniqueRawData.iloc[:,j+2],color='0.75',linewidth=0.2,label=UniqueRawData.iloc[:,j+2].name)
        ax11.set_xlabel(UniqueData.columns[j+2])
        ax22.set_xlabel(UniqueRawData.columns[j+2])
    ax3.plot(UniqueData['Time'],UniqueData.iloc[:,1],color='0.75',linewidth=0.2,label=UniqueData.iloc[:,1].name)
    ax4.plot(UniqueRawData['Time'],UniqueRawData.iloc[:,1],color='0.75',linewidth=0.2,label=UniqueRawData.iloc[:,1].name)
fig1.text(0.5,0.05,'Temperature without Noise',ha='center',va='center')
fig2.text(0.5,0.05,'Temperature with Noise',ha='center',va='center')
fig1.savefig('Temperature without Noise.svg',format = 'svg')
fig2.savefig('Temperature with Noise.svg',format = 'svg')
fig3.text(0.5,0.05,'AveTemperature without Noise',ha='center',va='center')
fig4.text(0.5,0.05,'AveTemperature with Noise',ha='center',va='center')
fig3.savefig('AveTemperature without Noise.svg',format = 'svg')
fig4.savefig('AveTemperature with Noise.svg',format = 'svg')    
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    