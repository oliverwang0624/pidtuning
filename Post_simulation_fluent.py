# -*- coding: utf-8 -*-
"""
Created on Fri Mar 16 16:12:02 2018

@author: Administrator
"""
import numpy as np
import sys
import os
from buildingspy.io.outputfile import Reader 
import pandas as pd
import shutil
from filter import modifiedEWMA as Mewma
from pso import pso
import matplotlib.pyplot as plt
import timeit
import winsound


def addUniformDistributedNoise(originDF,accuracy,exceptFirstOne=True):
    if exceptFirstOne:
        noise = np.random.uniform(-accuracy,accuracy,[np.size(originDF,axis=0)-1,np.size(originDF,axis=1)-1])
        addNoise = originDF.iloc[1:,1:np.size(originDF.columns)]+noise
        modifiedDF = pd.concat([originDF['Time'],addNoise],axis=1)
        modifiedDF.iloc[0,:] = originDF.iloc[0,:]
        return modifiedDF
    else:
        noise = np.random.uniform(-accuracy,accuracy,[np.size(originDF,axis=0),np.size(originDF,axis=1)-1])
        addNoise = originDF.iloc[:,1:np.size(originDF.columns)]+noise
        modifiedDF = pd.concat([originDF['Time'],addNoise],axis=1)
        return modifiedDF
    
def addNormalDistributedNoise(originDF,StD,exceptFirstOne=True):
    if exceptFirstOne:
        noise = np.random.normal(0,StD,[np.size(originDF,axis=0)-1,np.size(originDF,axis=1)-1])      
        addNoise = originDF.iloc[1:,1:np.size(originDF.columns)]+noise
        modifiedDF = pd.concat([originDF['Time'],addNoise],axis=1)
        modifiedDF.iloc[0,:] = originDF.iloc[0,:]
        return modifiedDF
    else:
        noise = np.random.normal(0,StD,[np.size(originDF,axis=0),np.size(originDF,axis=1)-1])
        addNoise = originDF.iloc[:,1:np.size(originDF.columns)]+noise
        modifiedDF = pd.concat([originDF['Time'],addNoise],axis=1)
        return modifiedDF

def constraintManual(x,*args):
    t1 = x[0]
    t2 = x[1]
    return [t1-t2]   
def constraint(x,*args):
    t1 = x[1]
    t2 = x[2]
    return [t1-t2]


def transferFunctionPython(repoPath,baselineValue,step,startTime,period,timestep,TF,ManualK=True,steadyPeriod=None):
    #Using another modelica model "Transfer function" to calculate the simulated output 
    #then using PSO to identify the proper parameters
    #repoPath is the directory of the repository
    #baselineValue is the raw data from coupled FFD simulation
    #step is the size of the step signal
    #startTime is the starting time when the excitation signal is introduced
    #period is the time period during which the identification is performed
    #timestep is the time step size of sampling
    K = []
    a = []
    b = []
    T1 = []
    T2 = []
    D = []
    Fopt = []
    ## Calculate the initial value at the startTime
    IniVal = baselineValue[baselineValue.Time==startTime].iloc[:,1].values
    if ManualK:
        #Calculate K manually
        if steadyPeriod == None:
            K=(baselineValue[(baselineValue.Time==startTime+period)].iloc[:,1].values-baselineValue[(baselineValue.Time==startTime)].iloc[:,1].values)/step
            K = K[-1]
        else:
            valueBefore = baselineValue[(baselineValue.Time==startTime)].iloc[:,1].values
            valueAfter = baselineValue[(baselineValue.Time>=startTime+period-steadyPeriod) & (baselineValue.Time<=startTime+period)].iloc[:,1].mean()
            K = (valueAfter-valueBefore)/step
        #Define the other parameters in the objective function
        args = (repoPath,baselineValue,K,startTime,period,timestep,step,TF)
        #Define the lower and upper boundary for the parameters
        if TF == 'FirstOrder':
            lb = [0.01]
            ub = [500]
        elif TF == 'Oscillation':
            lb = [0.01,0]
            ub = [500,1]
        elif TF == 'SecondOrder_2':
            lb = [0,0]
            ub = [500,500]
        elif TF == 'SecondOrderPlusDelay':
            lb = [0,0,0]
            ub = [500,500,100]
        
        for i in range(1): # Perform n times of PSO to get a best result
            #xopt,fopt = pso(objectiveFunctionDymola,lb,ub,f_ieqcons=constraint,args = args, debug=True)
            if TF == 'FirstOrder':
                xopt,fopt = pso(objectiveFunctionPythonManual,lb,ub,args = args)
                T1.append(xopt[0])
                Fopt.append(fopt)
            elif TF == 'Oscillation':
                xopt,fopt = pso(objectiveFunctionPythonManual,lb,ub,args = args)
                a.append(xopt[0])
                b.append(xopt[1])
                Fopt.append(fopt)
            elif TF == 'SecondOrder_2':
                xopt,fopt = pso(objectiveFunctionPythonManual,lb,ub,f_ieqcons = constraintManual,args = args)
                T1.append(xopt[0])
                T2.append(xopt[1])
                Fopt.append(fopt)
            elif TF == 'SecondOrderPlusDelay':
                xopt,fopt = pso(objectiveFunctionPythonManual,lb,ub,f_ieqcons = constraintManual,args = args)
                T1.append(xopt[0])
                T2.append(xopt[1])
                D.append(xopt[2])
                Fopt.append(fopt)
        Kopt = K[-1]
        if TF == 'FirstOrder':
            T1opt = T1[Fopt.index(min(Fopt))]
            fopt = min(Fopt)
            return Kopt, T1opt, fopt, IniVal
        elif TF == 'Oscillation':
            aopt = a[Fopt.index(min(Fopt))]
            bopt= b[Fopt.index(min(Fopt))]
            fopt = min(Fopt)
            return Kopt, aopt, bopt, fopt, IniVal
        elif TF == 'SecondOrder_2':
            T1opt = T1[Fopt.index(min(Fopt))]
            T2opt= T2[Fopt.index(min(Fopt))]
            fopt = min(Fopt)
            return Kopt, T1opt, T2opt, fopt, IniVal
        elif TF == 'SecondOrderPlusDelay':
            T1opt = T1[Fopt.index(min(Fopt))]
            T2opt= T2[Fopt.index(min(Fopt))]
            Dopt = D[Fopt.index(min(Fopt))]
            fopt = min(Fopt)
            return Kopt,T1opt,T2opt,Dopt,fopt, IniVal
    else:
        #Define the other parameters in the objective function
        args = (repoPath,baselineValue, startTime,period,timestep,step,TF)
        #Define the lower and upper boundary for the parameters
        if TF == 'FirstOrder':
            lb = [-10,0.01]
            ub = [-0.01,500]
        elif TF == 'Oscillation':
            lb = [-10,0.01,0]
            ub = [-0.01,500,1]
        elif TF == 'SecondOrder_2':
            lb = [-10,0,0]
            ub = [-0.01,500,500]
        elif TF == 'SecondOrderPlusDelay':
            lb = [-10,0,0,0]
            ub = [-0.01,500,500,100]
        
        for i in range(3): # Perform n times of PSO to get a best result
            #xopt,fopt = pso(objectiveFunctionDymola,lb,ub,f_ieqcons=constraint,args = args, debug=True)
            if TF == 'FirstOrder':
                xopt,fopt = pso(objectiveFunctionPython,lb,ub,args = args)
                K.append(xopt[0])
                T1.append(xopt[1])
                Fopt.append(fopt)
            elif TF == 'Oscillation':
                xopt,fopt = pso(objectiveFunctionPython,lb,ub,args = args)
                K.append(xopt[0])
                a.append(xopt[1])
                b.append(xopt[2])
                Fopt.append(fopt)
            elif TF == 'SecondOrder_2':
                xopt,fopt = pso(objectiveFunctionPython,lb,ub,f_ieqcons = constraint,args = args)
                K.append(xopt[0])
                T1.append(xopt[1])
                T2.append(xopt[2])
                Fopt.append(fopt)
            elif TF == 'SecondOrderPlusDelay':
                xopt,fopt = pso(objectiveFunctionPython,lb,ub,f_ieqcons = constraint,args = args)
                K.append(xopt[0])
                T1.append(xopt[1])
                T2.append(xopt[2])
                D.append(xopt[3])
                Fopt.append(fopt)
        Kopt = K[Fopt.index(min(Fopt))]    
        if TF == 'FirstOrder':
            T1opt = T1[Fopt.index(min(Fopt))]
            fopt = min(Fopt)
            return Kopt, T1opt, fopt, IniVal
        elif TF == 'Oscillation':
            aopt = a[Fopt.index(min(Fopt))]
            bopt= b[Fopt.index(min(Fopt))]
            fopt = min(Fopt)
            return Kopt, aopt, bopt, fopt, IniVal
        elif TF == 'SecondOrder_2':
            T1opt = T1[Fopt.index(min(Fopt))]
            T2opt= T2[Fopt.index(min(Fopt))]
            fopt = min(Fopt)
            return Kopt, T1opt, T2opt, fopt, IniVal
        elif TF == 'SecondOrderPlusDelay':
            T1opt = T1[Fopt.index(min(Fopt))]
            T2opt= T2[Fopt.index(min(Fopt))]
            Dopt = D[Fopt.index(min(Fopt))]
            fopt = min(Fopt)
            return Kopt,T1opt,T2opt,Dopt,fopt ,IniVal


def objectiveFunctionPythonManual(x,*args):
    
    repoPath, meaDF,K,startTime, period, timestep,step, TF = args
    
    ## Calculate the initial value at the startTime
    IniVal = meaDF[meaDF.Time==startTime].iloc[:,1].values
    # Create time series
    realtime = np.arange(startTime,startTime+period+timestep,timestep)
    timetag = realtime-startTime
    # Calculate corresponding output of transfer function
    # vectorize transfer function
    if TF == 'FirstOrder':
        T1 = x
        Vector = np.vectorize(FirstOrder,otypes=[np.float])
        output = Vector(K, T1, step, timetag)+IniVal
    elif TF == 'Oscillation':
        a, b  = x
        Vector = np.vectorize(Oscillation,otypes=[np.float])
        output = Vector(K, a, b, step, timetag)+IniVal
    elif TF == 'SecondOrder_2':
        T1, T2  = x
        Vector = np.vectorize(SecondOrder_2,otypes=[np.float])
        output = Vector(K, T1, T2, step, timetag)+IniVal
    elif TF == 'SecondOrderPlusDelay':
        T1, T2, D = x
        V2ndOrderPlusDelay = np.vectorize(SecondOrderPlusDelay,otypes=[np.float])
        output = V2ndOrderPlusDelay(K, T1, T2, D, step, timetag)+IniVal
    DF = {'Time':realtime,'simTemp':output}
    resultDF = pd.DataFrame(data = DF)

    #simDF and meaDF are both type of series
    #Slice the data after the startime and within the transition period
    simDF = resultDF[(resultDF.Time>=startTime)&(resultDF.Time<=startTime+period)]
    meaDF = meaDF[(meaDF.Time>=startTime)&(meaDF.Time<=startTime+period)]
    #Drop dupilicates
    simDF = simDF.drop_duplicates("Time")
    meaDF = meaDF.drop_duplicates("Time")
    if np.size(simDF,axis=0) != np.size(meaDF,axis=0):
        sys.exit("The dimensions of simDF and MeaDF don't match with each other!")
   
    #Calculate the error between the simulated value using transfer function and the coupled FFD simulation
    #Both the error of the points and the error of slope between two adjacent points are taken into account 
    absError = np.sum(np.square(simDF.iloc[:,1].values-meaDF.iloc[:,1].values))
#    slope_sim = np.zeros([np.size(simDF)-1,1])
#    slope_mea = np.zeros([np.size(meaDF)-1,1])
#    slope_sim = (simDF.iloc[1:,1].values-simDF.iloc[:-1,1].values)/timestep
#    slope_mea = (meaDF.iloc[1:,1].values-meaDF.iloc[:-1,1].values)/timestep
#    slopeError = np.sum(np.abs(slope_sim-slope_mea))
#    objVal = np.log(absError)+np.log(slopeError)
#    objVal = absError*slopeError
#    objVal = np.log(absError)
    return absError

def objectiveFunctionPython(x,*args):
    
    repoPath, meaDF,startTime, period, timestep,step, TF = args
    
    ## Calculate the initial value at the startTime
    IniVal = meaDF[meaDF.Time==startTime].iloc[:,1].values
    # Create time series
    realtime = np.arange(startTime,startTime+period+timestep,timestep)
    timetag = realtime-startTime
    # Calculate corresponding output of transfer function
    # vectorize transfer function
    if TF == 'FirstOrder':
        K, T1 = x
        Vector = np.vectorize(FirstOrder,otypes=[np.float])
        output = Vector(K, T1, step, timetag)+IniVal
    elif TF == 'Oscillation':
        K, a, b = x
        Vector = np.vectorize(Oscillation,otypes=[np.float])
        output = Vector(K, a, b, step, timetag)+IniVal
    elif TF == 'SecondOrder_2':
        K, T1, T2 = x
        Vector = np.vectorize(SecondOrder_2,otypes=[np.float])
        output = Vector(K, T1, T2, step, timetag)+IniVal
    elif TF == 'SecondOrderPlusDelay':
        K, T1, T2, D = x
        V2ndOrderPlusDelay = np.vectorize(SecondOrderPlusDelay,otypes=[np.float])
        output = V2ndOrderPlusDelay(K, T1, T2, D, step, timetag)+IniVal
    DF = {'Time':realtime,'simTemp':output}
    resultDF = pd.DataFrame(data = DF)

    #simDF and meaDF are both type of series
    #Slice the data after the startime and within the transition period
    simDF = resultDF[(resultDF.Time>=startTime)&(resultDF.Time<=startTime+period)]
    meaDF = meaDF[(meaDF.Time>=startTime)&(meaDF.Time<=startTime+period)]
    #Drop dupilicates
    simDF = simDF.drop_duplicates("Time")
    meaDF = meaDF.drop_duplicates("Time")
    if np.size(simDF,axis=0) != np.size(meaDF,axis=0):
        sys.exit("The dimensions of simDF and MeaDF don't match with each other!")
   
    #Calculate the error between the simulated value using transfer function and the coupled FFD simulation
    #Both the error of the points and the error of slope between two adjacent points are taken into account 
    absError = np.sum(np.square(simDF.iloc[:,1].values-meaDF.iloc[:,1].values))
#    slope_sim = np.zeros([np.size(simDF)-1,1])
#    slope_mea = np.zeros([np.size(meaDF)-1,1])
#    slope_sim = (simDF.iloc[1:,1].values-simDF.iloc[:-1,1].values)/timestep
#    slope_mea = (meaDF.iloc[1:,1].values-meaDF.iloc[:-1,1].values)/timestep
#    slopeError = np.sum(np.abs(slope_sim-slope_mea))
#    objVal = np.log(absError)+np.log(slopeError)
#    objVal = absError*slopeError
#    objVal = np.log(absError)
    return absError
    
def SecondOrderPlusDelay(K,T1,T2,D,step,time):
    if time <= D:
        output = 0
    else:
        if T2-T1 == 0 or T1 == 0 or T2== 0:
            output = 0
        else:
            output = step*K*(1+T1*T2/(T2-T1)*(np.exp(-(time-D)/T1)/T2-np.exp(-(time-D)/T2)/T1))
        
    return output

def FirstOrder(K,T1,step,time):
    output = step*K*(1-np.exp(-time/T1))
    return output

def Oscillation(K,T,eta,step,t):
#    output = step*K*(1-np.exp(-b*t/2*a)*(np.cosh((t*np.sqrt(np.square(b)/4-a))/a)+b*np.sinh((t*np.sqrt(np.square(b)/4-a))/a)/(2*np.sqrt(np.square(b)/4-a))))
    output = step*K*(1 - np.exp(-(eta*t)/T)*(np.cosh((t*(eta**2 - 1)**(1/2))/T) + (eta*np.sinh((t*(eta**2 - 1)**(1/2))/T))/((eta**2- 1)**(1/2))))

    return output

def SecondOrder_2(K,T1,T2,step,t):
    if T2-T1 == 0 or T1 == 0 or T2== 0:
        output = 0
    else:
        output = step*K*(1-np.exp(-t/T1)*T1/(T1-T2)+np.exp(-t/T2)*T2/(T1-T2))
    return output
        
def comparisonPython(step,meaEWMADF,meaNoiseDF,meaDF,startTime,period,timestep,TF,args=()):
    ## Calculate the initial value at the startTime
    IniVal = meaDF[meaDF.Time==startTime].iloc[:,1].values
    # Create time series
    realtime = np.arange(startTime,startTime+period+timestep,timestep)
    timetag = realtime-startTime
    # Calculate corresponding output of transfer function
    # vectorize transfer function
    if TF == 'FirstOrder':
        K, T1 = args
        Vector = np.vectorize(FirstOrder,otypes=[np.float])
        output = Vector(K, T1, step, timetag)+IniVal
    elif TF == 'Oscillation':
        K, a, b = args
        Vector = np.vectorize(Oscillation,otypes=[np.float])
        output = Vector(K, a, b, step, timetag)+IniVal
    elif TF == 'SecondOrder_2':
        K, T1, T2 = args
        Vector = np.vectorize(SecondOrder_2,otypes=[np.float])
        output = Vector(K, T1, T2, step, timetag)+IniVal
    elif TF == 'SecondOrderPlusDelay':
        K, T1, T2, D = args
        V2ndOrderPlusDelay = np.vectorize(SecondOrderPlusDelay,otypes=[np.float])
        output = V2ndOrderPlusDelay(K, T1, T2, D, step, timetag)+IniVal
   

    DF = {'Time':realtime,'simTemp':output}
    resultDF = pd.DataFrame(data = DF)
    
    #simDF and meaDF are both type of series
    #Slice the data after the startime and within the transition period
    simDF = resultDF[(resultDF.Time>=startTime)&(resultDF.Time<=startTime+period)]
    meaDF = meaDF[(meaDF.Time>=startTime)&(meaDF.Time<=startTime+period)]
    meaEWMADF = meaEWMADF[(meaEWMADF.Time>=startTime)&(meaEWMADF.Time<=startTime+period)]
    meaNoiseDF = meaNoiseDF[(meaNoiseDF.Time>=startTime)&(meaNoiseDF.Time<=startTime+period)]
    #Drop dupilicates
    simDF = simDF.drop_duplicates("Time")
    meaEWMADF = meaEWMADF.drop_duplicates("Time")
    meaDF = meaDF.drop_duplicates("Time")
    meaNoiseDF = meaNoiseDF.drop_duplicates("Time")
    if np.size(simDF,axis=0) != np.size(meaDF,axis=0):
        sys.exit("The dimensions of simDF and MeaDF don't match with each other!")
    
     # Plot simDF and meaDF
    fig,ax1 = plt.subplots(1,1,figsize=(6,4))
#    ax1=ax0[0]
    ax1.plot(simDF["Time"].values,simDF.iloc[:,1].values-273.15,'b',label = "Transfer Function #1",linewidth=1.2)
    ax1.plot(meaDF["Time"].values,meaDF.iloc[:,1].values-273.15,'r-.',label = "CFD w/o Noise",linewidth=1.2)
    ax1.plot(meaNoiseDF["Time"].values,meaNoiseDF.iloc[:,1].values-273.15,color='0.75',label = "CFD w Noise",linewidth=0.5)
#    ax2 = ax1.twinx()
#    ax2.plot(np.arange(startTime,startTime+period,timestep),0.24*np.ones_like(np.arange(startTime,startTime+period,timestep)),'g',label = 'Input Signal', linewidth=1.2)
#    ax2.plot(np.arange(startTime-10,startTime,timestep),0.018*np.ones_like(np.arange(startTime-10,startTime,timestep)),'g',linewidth=1.2)
#    ax2.plot(np.ones_like(np.arange(0.018,0.24,0.001)),np.arange(0.018,0.24,0.001),'g',linewidth=1.2)
#    ax.plot(meaEWMADF["Time"].values,meaEWMADF.iloc[:,1].values-273.15,'g--',label = "CFD w Noise after modified_EWMA",linewidth=0.8)
    
    ax1.legend(loc=0,fontsize=12)
    ax1.set_ylabel(meaDF.columns[1]+'/$^\circ$C',fontsize=15) 
    ax1.set_xlabel('Time/ s',fontsize=15)
#    ax2.set_ylabel('Supply airflow rate / kg/s',fontsize=15) 
#    ax2.set_yticks(np.arange(0.01,0.26,0.05))
#    ax2.legend(loc='best',fontsize=12)          
    fig.savefig(repoPath+'/Transfer Function Identification_fluent.svg',format='svg',dpi=1000)

def readoutfile(resultfile):
    time = []
    temp = []
    with open(resultfile,'r+') as myfile:
        for index, line in enumerate(myfile):
            if index < 3:
                continue
            else:
                time.append(line.split()[2])
                temp.append(line.split()[1])
    time = np.array(time,'float')
    temp = np.array(temp,'float')
    return time, temp

# Main script
if __name__ == "__main__":
    Start = timeit.default_timer()
    
    repoPath = os.getcwd()
    PackagePath = repoPath+'/ModelicaModels'
    ResultPath = repoPath+'/Results_fluent'
    
    ScenarioNum = 1
    Finished = 0
    #Options for start over or not
    startOver = raw_input('Are you sure you want to start over? Answer with y/n____________')
    if startOver == 'y':
        #Purge old results
        if os.path.exists(ResultPath+'/Summary'):
            shutil.rmtree(ResultPath+'/Summary')
            os.mkdir(ResultPath+'/Summary')
        else:
            os.mkdir(ResultPath+'/Summary')
    elif startOver == 'n':
        Finished = input('How many cases have been finished? Answer with Integers_____________')
        if type(Finished) != int:
            sys.exit('I said answer with integer!!!')   
    ## Read all mat results of different scenarios
    #Loop in scnearios
    for i in np.arange(Finished,Finished+ScenarioNum,1):
        #Creat dataframe for this scenario
        #Read time and temperature data
        ResultFile = os.path.join(ResultPath,"Scenario-"+str(i+1),
                                  "averagetemp-rfile.out")
        #Read temperature data without noise
        time,AveTemp = readoutfile(ResultFile)
        LocTemp = np.zeros([np.size(time,axis=0),16])
        #Loop in 16 local temperature sensors
        for j in range(16):
            time,loctemp = readoutfile(os.path.join(ResultPath,'Scenario-'+str(i+1),'temp'+str(j+1)+'-rfile.out'))
            LocTemp[:,j] = loctemp
        
        #Concat all the data into one dataframe
        timeDF = pd.DataFrame(time,columns=['Time'])
        AveTempDF = pd.DataFrame(AveTemp,columns=['AveTemp'])
        LocTempDF = pd.DataFrame(LocTemp,columns=['LocTemp1','LocTemp2','LocTemp3','LocTemp4',
                                                  'LocTemp5','LocTemp6','LocTemp7','LocTemp8',
                                                  'LocTemp9','LocTemp10','LocTemp11','LocTemp12',
                                                  'LocTemp13','LocTemp14','LocTemp15','LocTemp16'])
        Data = pd.concat([timeDF,AveTempDF,LocTempDF],axis=1)
            
        UniqueData = Data.drop_duplicates('Time')
        UniqueData.index = range(np.size(UniqueData,axis=0))
#        UniqueData.to_csv(ResultPath+'/Summary/Fluent Results_Scenario-'+str(i+1)+'.csv')
        UniqueRawData = addNormalDistributedNoise(UniqueData,0.55)
#        UniqueRawData = RawData.drop_duplicates('Time')
#        UniqueRawData.to_csv(ResultPath+'/Summary/Fluent Results_Scenario-'+str(i+1)+'_withNoise.csv')
        
        ##############################################################################
        #This section is to identify all the transfer functions
        ##Identify the parameters of transfer function
        # Create a dataframe to restore the transfer fucntion parameters
#        K = np.zeros([1,17])
#        T1 = np.zeros([1,17])
#        T2 = np.zeros([1,17])
#        IniVal = np.zeros([1,17])
#        K_DF = pd.DataFrame(K,columns=list(UniqueData.iloc[:,1:18]))
#        T1_DF = pd.DataFrame(T1,columns=list(UniqueData.iloc[:,1:18]))
#        T2_DF = pd.DataFrame(T2,columns=list(UniqueData.iloc[:,1:18]))
#        IniVal_DF = pd.DataFrame(IniVal,columns=list(UniqueData.iloc[:,1:18]))
#        parameters_DF = pd.concat([K_DF,T1_DF,T2_DF,IniVal_DF],axis=0)
#        parameters_DF.index = ['K','T1','T2','IniVal']
        ##################################################################################
        #This section is to identify the transfer function of average temperature only
        parameters_DF = pd.read_csv(os.path.join(repoPath,'Results_fluent','Summary','Transfer Function_Scenario-'+str(i+1)+'.csv')).iloc[:,1:]
        
        # Identify K, T1, T2, D
        for j in range(17):                   #the number of loop iteration is the number of sensors 17 in total
            #Filtering the raw temperature data
            EWMA_Temp = Mewma(UniqueRawData.iloc[:,j+1],0.29,alpha=0.4)
            EWMASeries = pd.concat([UniqueData['Time'],EWMA_Temp],axis=1)
            if j == 1:
                TF = 'SecondOrder_2'
                [k,T1,T2,fopt,iniVal] = transferFunctionPython(repoPath,UniqueRawData.iloc[:,[0,j+1]],0.04-0.018,0,1500,1,TF,ManualK=True,steadyPeriod=50)
                # Save P I into the dataframe
                parameters_DF['AveTemp']= [k,T1,T2,iniVal[0]]
                args = (k,T1,T2)
                comparisonPython(0.04-0.018,EWMASeries,UniqueRawData.iloc[:,[0,j+1]],UniqueData.iloc[:,[0,j+1]],0,1500,1,TF,args=args)
            else:
                continue
                TF = 'SecondOrder_2'
                [k,T1,T2,fopt,iniVal] = transferFunctionPython(repoPath,UniqueRawData.iloc[:,[0,j+1]],0.04-0.018,0,1500,1,TF,ManualK=True,steadyPeriod=50)
                # Save P I into the dataframe
                parameters_DF.iloc[:,j] = [k,T1,T2,iniVal[0]]
                args = (k,T1,T2)
                comparisonPython(0.04-0.018,EWMASeries,UniqueRawData.iloc[:,[0,j+1]],UniqueData.iloc[:,[0,j+1]],0,1500,1,TF,args=args)

    #    # Save PI_DF 
#        parameters_DF.to_csv(ResultPath+'/Summary/Transfer Function_Scenario-'+str(i+1)+'.csv')
    Time = timeit.default_timer()-Start

#duration = 1000
#freq = 900
#winsound.Beep(freq,duration)