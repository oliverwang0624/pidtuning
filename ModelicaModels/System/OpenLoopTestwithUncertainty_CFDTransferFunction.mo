within ModelicaModels.System;
model OpenLoopTestwithUncertainty_CFDTransferFunction
  extends Modelica.Icons.Example;
  package MediumA = Buildings.Media.Air (T_default=283.15) "Medium model";
  parameter String risTimInput=Modelica.Utilities.Files.loadResource("../Resources/InputFiles/risTimInput.txt");
  parameter String PLCUncInput=Modelica.Utilities.Files.loadResource("../Resources/InputFiles/PLCUncInput.txt");
  parameter String supTempInput=Modelica.Utilities.Files.loadResource("../Resources/InputFiles/supTempInput.txt");
  parameter String TransferFunInput=Modelica.Utilities.Files.loadResource("../Resources/InputFiles/TransferFunInput.txt");
  parameter Real UncRisTim(unit="s") = Modelica.Utilities.Examples.readRealParameter(risTimInput, "riseTime") "Uncertain rise time";
  parameter Modelica.SIunits.Temperature supTemp= Modelica.Utilities.Examples.readRealParameter(supTempInput, "supTemp");
  parameter Real UncFacPLC = Modelica.Utilities.Examples.readRealParameter(PLCUncInput, "UncFacPLC");
  parameter Real K = Modelica.Utilities.Examples.readRealParameter(TransferFunInput, "K")
    "Scale factor K";
  parameter Real T1 = Modelica.Utilities.Examples.readRealParameter(TransferFunInput, "T1")
    "1st Integral constant";
  parameter Real T2 = Modelica.Utilities.Examples.readRealParameter(TransferFunInput, "T2")
    "2nd Integral constant";
  parameter Real IniVal = Modelica.Utilities.Examples.readRealParameter(TransferFunInput, "InitialValue")
    "Delay time";



  Buildings.Fluid.Sources.FixedBoundary bouIn(
    redeclare package Medium = MediumA,
    use_T=true,
    nPorts=1,
    T=supTemp,
    p(displayUnit="Pa") = 101325 + 28)
              annotation (Placement(transformation(extent={{-108,56},{-88,76}})));
  Components.Fluid.Damper.VAVBoxExponential_newFilter_Unc                vavDam(
    redeclare package Medium = MediumA,
    riseTime=UncRisTim,
    y_start=0.3,
    m_flow_nominal=0.06,
    dp_nominal=28,
    v_nominal=3)
    annotation (Placement(transformation(extent={{-50,56},{-30,76}})));
  Modelica.Blocks.Continuous.FirstOrder firstOrder(
    T=T1,
    k=K,
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-30,6},{-10,26}})));
  Modelica.Blocks.Continuous.FirstOrder firstOrder1(
    T=max(T2,1e-8),
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{26,6},{46,26}})));
  Modelica.Blocks.Sources.RealExpression realExpression(y=-vavDam.port_b.m_flow
         - 0.018)
    annotation (Placement(transformation(extent={{-90,6},{-70,26}})));
  Modelica.Blocks.Sources.Constant Const(k=IniVal)
    "Initial value of the process"
    annotation (Placement(transformation(extent={{26,-30},{46,-10}})));
  Modelica.Blocks.Math.Add FinalValue
    annotation (Placement(transformation(extent={{90,0},{110,20}})));
  Buildings.Fluid.Sources.FixedBoundary bouIn1(
    redeclare package Medium = MediumA,
    nPorts=1,
    p(displayUnit="Pa") = 101325,
    use_T=false)
              annotation (Placement(transformation(extent={{20,56},{0,76}})));
  Modelica.Blocks.Sources.Step step(
    startTime=100,
    offset=0.3,
    height=0.6)
    annotation (Placement(transformation(extent={{-106,94},{-86,114}})));
equation
  connect(bouIn.ports[1],vavDam. port_a)
    annotation (Line(points={{-88,66},{-50,66}},  color={0,127,255}));
  connect(firstOrder.y,firstOrder1. u)
    annotation (Line(points={{-9,16},{24,16}},
                                           color={0,0,127}));
  connect(realExpression.y, firstOrder.u)
    annotation (Line(points={{-69,16},{-32,16}},color={0,0,127}));
  connect(Const.y,FinalValue. u2) annotation (Line(points={{47,-20},{70,-20},{
          70,4},{88,4}},     color={0,0,127}));
  connect(firstOrder1.y, FinalValue.u1)
    annotation (Line(points={{47,16},{88,16}}, color={0,0,127}));
  connect(vavDam.port_b, bouIn1.ports[1])
    annotation (Line(points={{-30,66},{0,66}}, color={0,127,255}));
  connect(step.y, vavDam.y)
    annotation (Line(points={{-85,104},{-40,104},{-40,78}}, color={0,0,127}));
   annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-120,-140},
            {200,120}})), Diagram(coordinateSystem(preserveAspectRatio=false,
          extent={{-120,-140},{200,120}})));
end OpenLoopTestwithUncertainty_CFDTransferFunction;
