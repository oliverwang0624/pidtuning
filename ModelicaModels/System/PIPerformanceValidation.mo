within ModelicaModels.System;
model PIPerformanceValidation "PI Performance Validation"
  extends Modelica.Icons.Example;
  package MediumA = Buildings.Media.Air (T_default=283.15) "Medium model";
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  parameter String TransferFunInput=Modelica.Utilities.Files.loadResource("../Resources/InputFiles/TransferFunInput_validation.txt");

  parameter Real K = Modelica.Utilities.Examples.readRealParameter(TransferFunInput, "K")
    "Scale factor K";
  parameter Real T1 = Modelica.Utilities.Examples.readRealParameter(TransferFunInput, "T1")
    "1st Integral constant";
  parameter Real T2 = Modelica.Utilities.Examples.readRealParameter(TransferFunInput, "T2")
    "2nd Integral constant";
  parameter Real D = Modelica.Utilities.Examples.readRealParameter(TransferFunInput, "D")
    "Delay time";
  parameter Real IniVal = Modelica.Utilities.Examples.readRealParameter(TransferFunInput, "IniVal")
    "Initial temperature";
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  parameter String TransferFunInputAve=Modelica.Utilities.Files.loadResource("../Resources/InputFiles/TransferFunInput_validationAve.txt");
  parameter Real KAve = Modelica.Utilities.Examples.readRealParameter(TransferFunInputAve, "KAve")
    "Scale factor K";
  parameter Real T1Ave = Modelica.Utilities.Examples.readRealParameter(TransferFunInputAve, "T1Ave")
    "1st Integral constant";
  parameter Real T2Ave = Modelica.Utilities.Examples.readRealParameter(TransferFunInputAve, "T2Ave")
    "2nd Integral constant";
  parameter Real DAve = Modelica.Utilities.Examples.readRealParameter(TransferFunInputAve, "DAve")
    "Delay time";
  parameter Real IniValAve = Modelica.Utilities.Examples.readRealParameter(TransferFunInputAve, "IniValAve")    "Initial temperature";
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 parameter String PIDInput=Modelica.Utilities.Files.loadResource("../Resources/InputFiles/PIDInput.txt");
 parameter Real Kp = Modelica.Utilities.Examples.readRealParameter(PIDInput, "Kp")
    "Proportional";
 parameter Real Ti = Modelica.Utilities.Examples.readRealParameter(PIDInput, "Ti")
    "Integration";
 parameter Real Td = Modelica.Utilities.Examples.readRealParameter(PIDInput, "Td")
    "Differentiation";
  parameter Modelica.Blocks.Types.SimpleController controllerType=Modelica.Blocks.Types.SimpleController.PID
    "Type of controller";

  Modelica.Blocks.Continuous.FirstOrder firstOrder(
    T=T1,
    k=K,
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-12,-18},{8,2}})));
  Modelica.Blocks.Continuous.FirstOrder secondOrder(
    T=T2,
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{24,-18},{44,2}})));
  Modelica.Blocks.Sources.Constant iniVal(k=IniVal)
    "Initial value of the process"
    annotation (Placement(transformation(extent={{42,-44},{62,-24}})));
  Modelica.Blocks.Math.Add FinalValue
    annotation (Placement(transformation(extent={{104,-24},{124,-4}})));
   Buildings.Controls.Continuous.LimPID conPID(
    reverseAction=true,
    k=abs(Kp),
    Ti=Ti,
    initType=Modelica.Blocks.Types.InitPID.SteadyState,
    Nd=10000,
    Ni=1/abs(Kp),
    Td=Td,
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    yMin=0)             annotation (Placement(transformation(extent={{-82,-10},
            {-62,10}})));
  Modelica.Blocks.Nonlinear.FixedDelay fixedDelay(delayTime=D)
    annotation (Placement(transformation(extent={{62,-18},{82,2}})));
  Modelica.Blocks.Sources.Constant Const1(k=0.3)
    "lower limit of damper position"
    annotation (Placement(transformation(extent={{-28,-46},{-48,-26}})));
  Modelica.Blocks.Math.Add minus(k2=-1)
    annotation (Placement(transformation(extent={{-46,-16},{-26,4}})));
  Components.Signal.DoubletStep Setpoint(
    offset=IniVal,
    height2=-3,
    startTime1st=100,
    height1=-1,
    startTime2nd=2100)
    annotation (Placement(transformation(extent={{-114,-10},{-94,10}})));
  Components.Signal.PlusNormallyDistributedError plusNormalNoise(sigma=0.55,
      samplePeriod=1)
    annotation (Placement(transformation(extent={{28,-80},{8,-60}})));
  Modelica.Blocks.Sources.RealExpression realExpression(y=minus.y)
    annotation (Placement(transformation(extent={{-30,88},{-10,108}})));
  Components.Room.TransferFuns transferFuns
    annotation (Placement(transformation(extent={{50,84},{78,114}})));
  Modelica.Blocks.Continuous.FirstOrder firstOrder1(
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0,
    k=KAve,
    T=T1Ave)
    annotation (Placement(transformation(extent={{-26,48},{-6,68}})));
  Modelica.Blocks.Continuous.FirstOrder secondOrder1(
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0,
    T=T2Ave)
    annotation (Placement(transformation(extent={{10,48},{30,68}})));
  Modelica.Blocks.Nonlinear.FixedDelay fixedDelay1(delayTime=DAve)
    annotation (Placement(transformation(extent={{48,48},{68,68}})));
  Modelica.Blocks.Sources.Constant iniVal1(k=IniValAve)
    "Initial value of the process"
    annotation (Placement(transformation(extent={{28,22},{48,42}})));
  Modelica.Blocks.Math.Add FinalValue1
    annotation (Placement(transformation(extent={{90,42},{110,62}})));
  Modelica.Blocks.Sources.RealExpression realExpression1(y=minus.y)
    annotation (Placement(transformation(extent={{-68,48},{-48,68}})));

equation
  connect(firstOrder.y,secondOrder. u)
    annotation (Line(points={{9,-8},{22,-8}},
                                           color={0,0,127}));
  connect(iniVal.y, FinalValue.u2) annotation (Line(points={{63,-34},{94,-34},{
          94,-20},{102,-20}},
                         color={0,0,127}));
  connect(secondOrder.y, fixedDelay.u)
    annotation (Line(points={{45,-8},{60,-8}}, color={0,0,127}));
  connect(fixedDelay.y, FinalValue.u1)
    annotation (Line(points={{83,-8},{102,-8}}, color={0,0,127}));
  connect(conPID.y, minus.u1) annotation (Line(points={{-61,0},{-48,0}},
                     color={0,0,127}));
  connect(Const1.y, minus.u2) annotation (Line(points={{-49,-36},{-60,-36},{-60,
          -12},{-48,-12}},
                     color={0,0,127}));
  connect(minus.y, firstOrder.u)
    annotation (Line(points={{-25,-6},{-20,-6},{-20,-8},{-14,-8}},
                                                 color={0,0,127}));
  connect(Setpoint.y, conPID.u_s)
    annotation (Line(points={{-93,0},{-84,0}},   color={0,0,127}));
  connect(FinalValue.y, plusNormalNoise.u[1]) annotation (Line(points={{125,-14},
          {142,-14},{142,-70},{30,-70}},color={0,0,127}));
  connect(plusNormalNoise.y[1], conPID.u_m)
    annotation (Line(points={{7,-70},{-72,-70},{-72,-12}}, color={0,0,127}));
  connect(realExpression.y, transferFuns.u) annotation (Line(points={{-9,98},{
          -4,98},{-4,97.5726},{49.0421,97.5726}},
                                              color={0,0,127}));
  connect(iniVal1.y, FinalValue1.u2) annotation (Line(points={{49,32},{80,32},{80,
          46},{88,46}}, color={0,0,127}));
  connect(fixedDelay1.y, FinalValue1.u1)
    annotation (Line(points={{69,58},{88,58}}, color={0,0,127}));
  connect(secondOrder1.y, fixedDelay1.u)
    annotation (Line(points={{31,58},{46,58}}, color={0,0,127}));
  connect(firstOrder1.y, secondOrder1.u)
    annotation (Line(points={{-5,58},{8,58}}, color={0,0,127}));
  connect(realExpression1.y, firstOrder1.u)
    annotation (Line(points={{-47,58},{-28,58}}, color={0,0,127}));
   annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-120,-140},
            {200,120}})), Diagram(coordinateSystem(preserveAspectRatio=false,
          extent={{-120,-140},{200,120}})));
end PIPerformanceValidation;
