within ModelicaModels.System;
model OpenLoopTestwithUncertainty_readExtTXT
  "simulate mixed convection in a room with a box inside"
  extends Modelica.Icons.Example;
  //Read external uncertainty parameters
  parameter String risTimInput=Modelica.Utilities.Files.loadResource("../Resources/InputFiles/risTimInput.txt");
  parameter String PLCUncInput=Modelica.Utilities.Files.loadResource("../Resources/InputFiles/PLCUncInput.txt");
  parameter String bouTemInput=Modelica.Utilities.Files.loadResource("../Resources/InputFiles/bouTemInput.txt");

  parameter Real UncRisTim(unit="s") = Modelica.Utilities.Examples.readRealParameter(risTimInput, "riseTime") "Uncertain rise time";
  parameter Real UncFacPLC = Modelica.Utilities.Examples.readRealParameter(PLCUncInput, "UncFacPLC")
    "Uncertainty factor,multiplied by the original flow coefficient of damper";
  parameter Modelica.SIunits.Temperature UncTOthWal(displayUnit="degC") = Modelica.Utilities.Examples.readRealParameter(bouTemInput, "Wall")
    "Fixed temperature at port";
  parameter Modelica.SIunits.Temperature UncTFlo(displayUnit="degC") = Modelica.Utilities.Examples.readRealParameter(bouTemInput, "Floor")
    "Fixed temperature at port";
  parameter Modelica.SIunits.Temperature UncTCei(displayUnit="degC") = Modelica.Utilities.Examples.readRealParameter(bouTemInput, "Ceiling")
    "Fixed temperature at port";
extends Components.Room.MixedConvectionBase(
    roo(
      surBou(
        name={"East Wall","West Wall","North Wall","South Wall","Ceiling","Floor"},
        A={5.7584,5.8804,5.9536,5.9536,5.9536,5.9536},
        til={Buildings.Types.Tilt.Wall,Buildings.Types.Tilt.Wall,Buildings.Types.Tilt.Wall,
            Buildings.Types.Tilt.Wall,Buildings.Types.Tilt.Ceiling,Buildings.Types.Tilt.Floor},
        each boundaryCondition=Buildings.ThermalZones.Detailed.Types.CFDBoundaryConditions.Temperature),
      cfdFilNam="Resources/Data/Rooms/FFD/MixedConvectionWithBox16sensors.ffd",
      intConMod=Buildings.HeatTransfer.Types.InteriorConvection.Fixed,
      sensorName={"occupant average T","occupant average V","thermostat1",
      "thermostat2","thermostat3","thermostat4","thermostat5","thermostat6",
      "thermostat7","thermostat8","thermostat9","thermostat10","thermostat11",
      "thermostat12","thermostat13","thermostat14","thermostat15","thermostat16"},
      p_start(displayUnit="Pa") = 101325,
      nPorts=2,
      samplePeriod=5,
      massDynamics=Modelica.Fluid.Types.Dynamics.SteadyStateInitial),
    nSurBou=6,
    nOthWal=4,
    TOthWal(each T(displayUnit="degC") = UncTOthWal),
    TFlo(T=UncTFlo),
    bouOut(             p(displayUnit="Pa"), use_T=false));

   Buildings.HeatTransfer.Sources.FixedTemperature Tcei(T(displayUnit="degC")=
         UncTCei)
    "temperature of ceiling"                                            annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=0,
        origin={188,-144})));

  Buildings.Fluid.Sources.FixedBoundary bouIn(
    redeclare package Medium = MediumA,
    use_T=true,
    nPorts=1,
    p(displayUnit="Pa") = 101325 + 19,
    T=289.15) annotation (Placement(transformation(extent={{-12,-122},{8,-102}})));
  ModelicaModels.Components.Fluid.Damper.VAVBoxExponential_newFilter_Unc vavDam(
    redeclare package Medium = MediumA,
    riseTime=UncRisTim,
    y_start=0,
    v_nominal=1,
    m_flow_nominal=0.1,
    dp_nominal=19)
    annotation (Placement(transformation(extent={{30,-122},{50,-102}})));

  Components.Signal.PlusNormallyDistributedError plusNormallyDistributedError(
    sigma=0.55,
    samplePeriod=roo.samplePeriod,
    nInputs=18)
    annotation (Placement(transformation(extent={{178,-36},{198,-16}})));


  Components.Signal.BumpStep bumpStep(height=-0.7, offset=1,
    startTime2nd=1400)
    annotation (Placement(transformation(extent={{-78,-104},{-58,-84}})));
equation
  connect(Tcei.port, roo.surf_surBou[5]) annotation (Line(
      points={{178,-144},{134.2,-144},{134.2,-30}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(bouIn.ports[1], vavDam.port_a)
    annotation (Line(points={{8,-112},{30,-112}}, color={0,127,255}));
  connect(vavDam.port_b, roo.ports[1]) annotation (Line(points={{50,-112},{74,-112},{74,-26},{123,-26}},
                               color={0,127,255}));
  connect(roo.yCFD[1:18], plusNormallyDistributedError.u[1:18]) annotation (Line(points={{159,
          -4},{162,-4},{162,-26},{176,-26}}, color={0,0,127}));
  connect(bumpStep.y, vavDam.y)
    annotation (Line(points={{-57,-94},{40,-94},{40,-100}}, color={0,0,127}));
  annotation (
    Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-120,-160},{240,140}})),
            experiment(
      StopTime=1500,
      Interval=5,
      __Dymola_Algorithm="Dassl"),
   Documentation(info="<html> 
<p align=\"left\">
</html>", revisions="<html>
<ul>
<li>   
December 31, 2013, by Wangda Zuo:<br/>
First implementation.
</li>
</ul>
</html>"),
    Icon(coordinateSystem(extent={{-120,-160},{240,140}})),
    __Dymola_experimentSetupOutput);
end OpenLoopTestwithUncertainty_readExtTXT;
