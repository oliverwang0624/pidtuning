within ModelicaModels.System;
model OpenLoopTestwithUncertainty
  "simulate mixed convection in a room with a box inside"
  extends Modelica.Icons.Example;
  parameter Real UncRisTim=100 "Uncertain rise time";
  parameter Real UncFacPLC=1
    "Uncertainty factor,multiplied by the original flow coefficient of damper";
  parameter Modelica.SIunits.Temperature UncTOthWal(displayUnit="degC") = 300.55
    "Fixed temperature at port";
  parameter Modelica.SIunits.Temperature UncTFlo(displayUnit="degC")=300.05
    "Fixed temperature at port";
  parameter Modelica.SIunits.Temperature UncTei(displayUnit="degC") = 298.95
    "Fixed temperature at port";
   extends Components.Room.MixedConvectionBase(
    roo(
      surBou(
        name={"East Wall","West Wall","North Wall","South Wall","Ceiling","Floor"},
        A={5.7584,5.8804,5.9536,5.9536,5.9536,5.9536},
        til={Buildings.Types.Tilt.Wall,Buildings.Types.Tilt.Wall,Buildings.Types.Tilt.Wall,
            Buildings.Types.Tilt.Wall,Buildings.Types.Tilt.Ceiling,Buildings.Types.Tilt.Floor},
        each boundaryCondition=Buildings.ThermalZones.Detailed.Types.CFDBoundaryConditions.Temperature),
      cfdFilNam="Resources/Data/Rooms/FFD/MixedConvectionWithBox.ffd",
      intConMod=Buildings.HeatTransfer.Types.InteriorConvection.Fixed,
      sensorName={"occupant average T","occupant average V","thermostat"},
      massDynamics=Modelica.Fluid.Types.Dynamics.SteadyStateInitial,
      p_start(displayUnit="Pa") = 101325 + 20,
      nPorts=2,
      samplePeriod=5),
    nSurBou=6,
    nOthWal=4,
    TOthWal(each T(displayUnit="degC") = UncTOthWal),
    TFlo(T=UncTFlo),
    bouOut(use_T=false, p(displayUnit="Pa")));

   Buildings.HeatTransfer.Sources.FixedTemperature Tcei(T(displayUnit="degC")=
         UncTei)
    "temperature of ceiling"                                            annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=0,
        origin={188,-144})));

  Components.Signal.BumpStep BumpStep(
    startTime1st=100,
    startTime2nd=200,
    height=0,
    offset=1)
    annotation (Placement(transformation(extent={{-80,-100},{-60,-80}})));
  Buildings.Fluid.Sources.FixedBoundary bouIn(
    redeclare package Medium = MediumA,
    use_T=true,
    p(displayUnit="Pa") = 101325 + 20,
    T=289.15,
    nPorts=1) annotation (Placement(transformation(extent={{-12,-122},{8,-102}})));
  ModelicaModels.Components.Fluid.Damper.VAVBoxExponential_newFilter_Unc vavDam(
    redeclare package Medium = MediumA,
    m_flow_nominal=0.09952,
    dp_nominal=20,
    v_nominal=3,
    UncRisTim=UncRisTim,
    UncFacPLC=UncFacPLC,
    y_start=0)
    annotation (Placement(transformation(extent={{30,-122},{50,-102}})));


  Components.Signal.PlusNormallyDistributedError plusNormallyDistributedError(
      nInputs=1, samplePeriod=roo.samplePeriod,
    sigma=0.5)
    annotation (Placement(transformation(extent={{178,-36},{198,-16}})));
equation
  connect(Tcei.port, roo.surf_surBou[5]) annotation (Line(
      points={{178,-144},{134.2,-144},{134.2,-30}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(bouIn.ports[1], vavDam.port_a)
    annotation (Line(points={{8,-112},{30,-112}}, color={0,127,255}));
  connect(vavDam.port_b, roo.ports[1]) annotation (Line(points={{50,-112},{86,-112},
          {86,-26},{123,-26}}, color={0,127,255}));
  connect(BumpStep.y, vavDam.y)
    annotation (Line(points={{-59,-90},{40,-90},{40,-100}}, color={0,0,127}));
  connect(roo.yCFD[3], plusNormallyDistributedError.u[1]) annotation (Line(points={{159,
          -4},{162,-4},{162,-26},{176,-26}}, color={0,0,127}));
  annotation (
    Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-120,-160},{240,140}})),
            experiment(StopTime=40),
   Documentation(info="<html> 
<p align=\"left\">
</html>", revisions="<html>
<ul>
<li>   
December 31, 2013, by Wangda Zuo:<br/>
First implementation.
</li>
</ul>
</html>"),
    Icon(coordinateSystem(extent={{-120,-160},{240,140}})));
end OpenLoopTestwithUncertainty;
