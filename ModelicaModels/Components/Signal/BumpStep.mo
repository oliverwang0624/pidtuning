within ModelicaModels.Components.Signal;
model BumpStep "Bump Stepinput signal"

    Modelica.Blocks.Sources.Step step(
    offset=0,
    height=height,
    startTime=startTime1st)
                   annotation (Placement(transformation(extent={{-44,6},{-24,26}})));
  Buildings.Controls.OBC.CDL.Continuous.Add add2
    annotation (Placement(transformation(extent={{4,-10},{24,10}})));
  Modelica.Blocks.Sources.Step step1(
    height=-height,
    offset=offset,
    startTime=startTime2nd)
                   annotation (Placement(transformation(extent={{-44,-28},{-24,
            -8}})));
  parameter Real height=0.7 "Height of step between startTime1st and startTime2nd";
  parameter Modelica.SIunits.Time startTime1st=400
    "Output y = offset for time < startTime1st";
  parameter Modelica.SIunits.Time startTime2nd=800
    "Output y = offset for time > startTime2nd";
  parameter Real offset=0.3 "Offset of output signal y";
  Modelica.Blocks.Interfaces.RealOutput y
    annotation (Placement(transformation(extent={{100,-10},{120,10}})));
equation
  connect(step.y, add2.u1) annotation (Line(points={{-23,16},{-12,16},{-12,6},{
          2,6}},                                                                      color={0,0,127}));
  connect(step1.y, add2.u2) annotation (Line(points={{-23,-18},{-12,-18},{-12,
          -6},{2,-6}},                                                                     color={0,0,127}));
  connect(add2.y, y)
    annotation (Line(points={{25,0},{110,0}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-66,152},{72,88}},
          lineColor={0,0,255},
          textString="BumpStep"),
        Line(
          points={{-94,-42},{-42,-42},{-42,54},{44,54},{44,-42},{96,-42}},
          color={0,0,0},
          thickness=0.5)}),                                      Diagram(coordinateSystem(preserveAspectRatio=false)));
end BumpStep;
