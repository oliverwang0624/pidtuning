within ModelicaModels.Components.Signal;
model DoubletStep "Doublet Stepinput signal"

    Modelica.Blocks.Sources.Step step(
    offset=0,
    startTime=startTime1st,
    height=height1)
                   annotation (Placement(transformation(extent={{-60,12},{-40,32}})));
  Buildings.Controls.OBC.CDL.Continuous.Add add2
    annotation (Placement(transformation(extent={{2,-10},{22,10}})));
  Modelica.Blocks.Sources.Step step1(
    offset=offset,
    startTime=startTime2nd,
    height=-height1 + height2)
                   annotation (Placement(transformation(extent={{-60,-38},{-40,-18}})));
  parameter Real height1=0.7 "Height of the first step";
  parameter Real height2=0.3 "Height of the second step";
  parameter Modelica.SIunits.Time startTime1st=400
    "Output y = height1+offset for (startTime1st =< time < startTime2nd)";
  parameter Modelica.SIunits.Time startTime2nd=800
    "Output y = height2+offset for time >= startTime2nd";
  parameter Real offset=0.3 "Offset of output signal y";
  Modelica.Blocks.Interfaces.RealOutput y
    annotation (Placement(transformation(extent={{100,-10},{120,10}})));
equation
  connect(step.y, add2.u1) annotation (Line(points={{-39,22},{-26,22},{-26,6},{0,6}}, color={0,0,127}));
  connect(step1.y, add2.u2) annotation (Line(points={{-39,-28},{-26,-28},{-26,-6},{0,-6}}, color={0,0,127}));
  connect(y, y) annotation (Line(points={{110,0},{110,0}}, color={0,0,127}));
  connect(add2.y, y) annotation (Line(points={{23,0},{110,0}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Rectangle(
          extent={{-100,100},{100,-100}},
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Text(
          extent={{-66,152},{72,88}},
          lineColor={0,0,255},
          textString="%name
"),     Line(
          points={{-94,-42},{-42,-42},{-42,54},{44,54},{44,-42},{96,-42}},
          color={0,0,0},
          thickness=0.5)}),                                      Diagram(coordinateSystem(preserveAspectRatio=false)));
end DoubletStep;
