within ModelicaModels.Components.Signal.BaseClasses;
block ActuatorSignalConstantSpeed "Acutator changing at constant speed"
 import Modelica.Blocks.Types.Init;
  parameter Real Risetime(unit="s")=1 "Rise time for actuator to move from 0 to 1";

  /* InitialState is the default, because it was the default in Modelica 2.2
     and therefore this setting is backward compatible
  */
  parameter Modelica.Blocks.Types.Init initType=Modelica.Blocks.Types.Init.InitialState
    "Type of initialization (1: no init, 2: steady state, 3,4: initial output)"     annotation(Evaluate=true,
      Dialog(group="Initialization"));
  parameter Real y_start=0 "Initial or guess value of output (= state)"
    annotation (Dialog(group="Initialization"));
  extends Modelica.Blocks.Interfaces.SISO(y(start=y_start));

initial equation
  if initType == Init.SteadyState then
     der(y) = 0;
  elseif initType == Init.InitialState or
         initType == Init.InitialOutput then
    y = y_start;
  end if;
equation
    der(y) = max(sign(abs(u-y)-0.0001),0)*sign(u-y)/Risetime;

  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end ActuatorSignalConstantSpeed;
