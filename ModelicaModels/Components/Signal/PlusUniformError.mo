within ModelicaModels.Components.Signal;
model PlusUniformError
  parameter Integer nInputs = 1 "Dimension of input signal";
  extends Modelica.Blocks.Interfaces.MIMO(final nin=nInputs, final nout=nin);
  parameter Real y_min "Lower limit of y";
  parameter Real y_max "Upper limit of y";
  parameter Modelica.SIunits.Period samplePeriod=1
    "Period for sampling the raw random numbers";

  inner Modelica.Blocks.Noise.GlobalSeed globalSeed
    annotation (Placement(transformation(extent={{20,30},{40,50}})));
  Modelica.Blocks.Noise.UniformNoise uniformNoise(
    y_min=y_min,
    y_max=y_max,
    samplePeriod=samplePeriod)
    annotation (Placement(transformation(extent={{-44,32},{-24,52}})));

equation
  for i in 1:nInputs loop
    y[i] = u[i]+uniformNoise.y;
  end for;
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end PlusUniformError;
