within ModelicaModels.Components.Fluid.Example;
model VAVBoxExponential_NewFilter_Unc
  extends Modelica.Icons.Example;
  replaceable package MediumA = Buildings.Media.Air;
  Damper.VAVBoxExponential_newFilter_Unc vavDam(
    redeclare package Medium = MediumA,
    m_flow_nominal=1,
    dp_nominal=20,
    y_start=0)
    annotation (Placement(transformation(extent={{-8,-10},{12,10}})));
  Buildings.Fluid.Sources.MassFlowSource_T boundary(nPorts=1,
    redeclare package Medium = MediumA,
    m_flow=1)
    annotation (Placement(transformation(extent={{-64,-10},{-44,10}})));
  Buildings.Fluid.Sources.FixedBoundary bou(nPorts=1, redeclare package Medium =
        MediumA)
    annotation (Placement(transformation(extent={{64,-10},{44,10}})));
  Signal.BumpStep BumpStep(
    height=1,
    startTime1st=100,
    startTime2nd=800,
    offset=0) annotation (Placement(transformation(extent={{-62,30},{-42,50}})));
equation
  connect(boundary.ports[1], vavDam.port_a)
    annotation (Line(points={{-44,0},{-8,0}}, color={0,127,255}));
  connect(vavDam.port_b, bou.ports[1])
    annotation (Line(points={{12,0},{44,0}}, color={0,127,255}));
  connect(BumpStep.y, vavDam.y)
    annotation (Line(points={{-41,40},{2,40},{2,12}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end VAVBoxExponential_NewFilter_Unc;
