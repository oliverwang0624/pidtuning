within ModelicaModels.Components.Fluid.Example;
model BranchResistance_InconstantLPLC
    "Example model for inconstant LPLC with nominal pressure drop as parameter"
    import Buildings;
    extends Modelica.Icons.Example;

   package Medium = Buildings.Media.Air "Medium model";

    Modelica.Blocks.Sources.Ramp M(
      duration=1,
      offset=1.2,
      height=0.5)
                "Ramp mass flow rate signal"
      annotation (Placement(transformation(extent={{-92,-2},{-72,18}})));

    Buildings.Fluid.Sources.Boundary_pT sin(
      redeclare package Medium = Medium,
      T=273.15 + 10,
      p(displayUnit="Pa") = 101325,
      nPorts=1) "Pressure boundary condition"
      annotation (Placement(transformation(extent={{92,-10},{72,10}})));

    Buildings.Fluid.Sensors.Velocity senVel(
      redeclare package Medium = Medium,
      m_flow_nominal=1,
      A=1,
      tau=0)
           annotation (Placement(transformation(extent={{-24,-10},{-4,10}})));
    Buildings.Fluid.Sensors.Velocity senVel1(
      redeclare package Medium = Medium,
      m_flow_nominal=1,
      A=1,
      tau=0)
      annotation (Placement(transformation(extent={{34,-10},{54,10}})));
    Buildings.Fluid.Sources.MassFlowSource_T boundary(
      use_m_flow_in=true,
      nPorts=1,
      redeclare package Medium = Medium) annotation (Placement(
          transformation(extent={{-52,-10},{-32,10}})));
  Junction.BranchResistance_IncLPLC branchResistance_inconstantLPLC(
    redeclare package Medium = Medium,
    PolyCoef={0.4526,-0.9833,0.2801,0.2785},
    m_flow_nominal=1.2,
    dp_nominal=1)
    annotation (Placement(transformation(extent={{5,-10},{25,10}})));
    Buildings.Fluid.FixedResistances.PressureDrop res(
      redeclare package Medium = Medium,
      deltaM=0.1,
      m_flow_nominal=1.2,
      dp_nominal=0.01674)
      annotation (Placement(transformation(extent={{6,-54},{26,-34}})));
    Buildings.Fluid.Sources.Boundary_pT sin1(
      redeclare package Medium = Medium,
      T=273.15 + 10,
      p(displayUnit="Pa") = 101325,
      nPorts=1) "Pressure boundary condition"
      annotation (Placement(transformation(extent={{94,-54},{74,-34}})));
    Buildings.Fluid.Sources.MassFlowSource_T boundary1(
      use_m_flow_in=true,
      nPorts=1,
      redeclare package Medium = Medium) annotation (Placement(
          transformation(extent={{-52,-54},{-32,-34}})));
equation
    connect(senVel1.port_b, sin.ports[1]) annotation (Line(points={{54,0},{64,
            0},{72,0}},        color={0,127,255}));
    connect(boundary.ports[1], senVel.port_a) annotation (Line(points={{-32,0},
            {-32,0},{-24,0}},         color={0,127,255}));
    connect(boundary.m_flow_in,M. y) annotation (Line(points={{-52,8},{-52,8},
            {-71,8}},        color={0,0,127}));
    connect(branchResistance_inconstantLPLC.port_a, senVel.port_b)
      annotation (Line(points={{5,0},{1,0},{-4,0}}, color={0,127,255}));
    connect(branchResistance_inconstantLPLC.port_b, senVel1.port_a)
      annotation (Line(points={{25,0},{34,0}}, color={0,127,255}));
    connect(senVel.v, branchResistance_inconstantLPLC.V_up) annotation (Line(
          points={{-14,11},{-14,20},{-14,23},{7.1,23},{7.1,5.1}}, color={0,0,127}));
    connect(senVel1.v, branchResistance_inconstantLPLC.V_down) annotation (Line(
          points={{44,11},{44,22},{44,23},{23.1,23},{23.1,5.1}}, color={0,0,127}));

    connect(res.port_b, sin1.ports[1]) annotation (Line(points={{26,-44},{50,
            -44},{74,-44}}, color={0,127,255}));
    connect(boundary1.ports[1], res.port_a) annotation (Line(points={{-32,-44},
            {-14,-44},{6,-44}}, color={0,127,255}));
    connect(boundary1.m_flow_in, M.y) annotation (Line(points={{-52,-36},{-66,
            -36},{-66,2},{-66,8},{-71,8}}, color={0,0,127}));
end BranchResistance_InconstantLPLC;
