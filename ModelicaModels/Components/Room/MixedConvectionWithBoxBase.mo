within ModelicaModels.Components.Room;
model MixedConvectionWithBoxBase
  "simulate mixed convection in a room with a box inside"

   extends Room.MixedConvectionBase(
     roo(
       surBou(
         name={"East Wall","West Wall","North Wall","South Wall","Ceiling","Floor"},
         A={5.7584,5.8804,5.9536,5.9536,5.9536,5.9536},
         til={Buildings.Types.Tilt.Wall,
             Buildings.Types.Tilt.Wall,
             Buildings.Types.Tilt.Wall,
             Buildings.Types.Tilt.Wall,
             Buildings.Types.Tilt.Ceiling,
             Buildings.Types.Tilt.Floor},
         each boundaryCondition=Buildings.ThermalZones.Detailed.Types.CFDBoundaryConditions.Temperature),
       cfdFilNam="Resources/Data/Rooms/FFD/MixedConvectionWithBox.ffd",
      intConMod=Buildings.HeatTransfer.Types.InteriorConvection.Fixed,
      samplePeriod=4,
      sensorName={"occupant average T","occupant average V","thermostat"},
      massDynamics=Modelica.Fluid.Types.Dynamics.SteadyStateInitial,
      p_start(displayUnit="Pa") = 101325 + 20,
      nPorts=2),
     nSurBou=6,
     nOthWal=4,
     TOthWal(each T(displayUnit="degC") = 300.55),
     TFlo(T=300.05),
    bouOut(                     use_T=false, p(displayUnit="Pa")));
   Buildings.HeatTransfer.Sources.FixedTemperature Tcei(T(displayUnit="degC")=
       298.95)
    "temperature of ceiling"                                            annotation (Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=0,
        origin={188,-144})));

  Signal.BumpStep doubletStep
    annotation (Placement(transformation(extent={{-80,-100},{-60,-80}})));
  Buildings.Fluid.Sources.FixedBoundary bouIn(
    nPorts=1,
    redeclare package Medium = MediumA,
    use_T=true,
    p(displayUnit="Pa") = 101325 + 20,
    T=289.15) annotation (Placement(transformation(extent={{-12,-122},{8,-102}})));
  Buildings.Fluid.Actuators.Dampers.VAVBoxExponential vavDam(
    redeclare package Medium = MediumA,
    m_flow_nominal=0.1,
    dp_nominal=20,
    v_nominal=3) annotation (Placement(transformation(extent={{28,-122},{48,-102}})));
equation
  connect(Tcei.port, roo.surf_surBou[5]) annotation (Line(
      points={{178,-144},{134.2,-144},{134.2,-30}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(bouIn.ports[1], vavDam.port_a) annotation (Line(points={{8,-112},{28,-112}}, color={0,127,255}));
  connect(vavDam.port_b, roo.ports[1])
    annotation (Line(points={{48,-112},{86,-112},{86,-26},{123,-26}}, color={0,127,255}));
  connect(doubletStep.y, vavDam.y) annotation (Line(points={{-59,-90},{38,-90},{
          38,-100}},                                                                       color={0,0,127}));
  annotation (
    Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-120,-160},{240,140}})),
            experiment(StopTime=40),
   Documentation(info="<html> 
<p align=\"left\">
</html>", revisions="<html>
<ul>
<li>   
December 31, 2013, by Wangda Zuo:<br/>
First implementation.
</li>
</ul>
</html>"),
    Icon(coordinateSystem(extent={{-120,-160},{240,140}})));
end MixedConvectionWithBoxBase;
