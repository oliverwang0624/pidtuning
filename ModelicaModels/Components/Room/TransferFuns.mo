within ModelicaModels.Components.Room;
model TransferFuns
  "Vectorized transfer functions of all local temperatures"
  parameter String AllTraFuns=Modelica.Utilities.Files.loadResource("../Resources/InputFiles/AllTransferFunctions.mat") "The mat file of all the transfer functions";
  final parameter String matrixName1 = "allTransFuns" "Names of the matrix";
  final parameter Integer dim[2]=Modelica.Utilities.Streams.readMatrixSize(AllTraFuns, matrixName1)  "Dimension of matrix";

  final parameter Real coeff[dim[1],dim[2]]= Modelica.Utilities.Streams.readRealMatrix(
      AllTraFuns,
      matrixName1,
      dim[1],
      dim[2])    "Matrix data";

  Modelica.Blocks.Continuous.FirstOrder firstOrder(
    T=coeff[2,1],
    k=coeff[1,1],
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-116,640},{-96,660}})));
  Modelica.Blocks.Continuous.FirstOrder secondOrder(
    T=coeff[3,1],
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-80,640},{-60,660}})));
  Modelica.Blocks.Nonlinear.FixedDelay fixedDelay(delayTime=coeff[4,1])
    annotation (Placement(transformation(extent={{-42,640},{-22,660}})));
  Modelica.Blocks.Sources.Constant iniVal(k=coeff[5,1])
    "Initial value of the process"
    annotation (Placement(transformation(extent={{-42,608},{-22,628}})));
  Modelica.Blocks.Math.Add FinalValue
    annotation (Placement(transformation(extent={{0,634},{20,654}})));
  Modelica.Blocks.Continuous.FirstOrder firstOrder1(
    T=coeff[2,2],
    k=coeff[1,2],
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-116,566},{-96,586}})));
  Modelica.Blocks.Continuous.FirstOrder secondOrder1(
    T=coeff[3,2],
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-80,566},{-60,586}})));
  Modelica.Blocks.Nonlinear.FixedDelay fixedDelay1(delayTime=coeff[4,2])
    annotation (Placement(transformation(extent={{-42,566},{-22,586}})));
  Modelica.Blocks.Sources.Constant iniVal1(k=coeff[5,2])
    "Initial value of the process"
    annotation (Placement(transformation(extent={{-42,534},{-22,554}})));
  Modelica.Blocks.Math.Add FinalValue1
    annotation (Placement(transformation(extent={{0,560},{20,580}})));
  Modelica.Blocks.Continuous.FirstOrder firstOrder2(
    T=coeff[2,3],
    k=coeff[1,3],
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-116,496},{-96,516}})));
  Modelica.Blocks.Continuous.FirstOrder secondOrder2(
    T=coeff[3,3],
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-80,496},{-60,516}})));
  Modelica.Blocks.Nonlinear.FixedDelay fixedDelay2(delayTime=coeff[4,3])
    annotation (Placement(transformation(extent={{-42,496},{-22,516}})));
  Modelica.Blocks.Sources.Constant iniVal2(k=coeff[5,3])
    "Initial value of the process"
    annotation (Placement(transformation(extent={{-42,464},{-22,484}})));
  Modelica.Blocks.Math.Add FinalValue2
    annotation (Placement(transformation(extent={{0,490},{20,510}})));
  Modelica.Blocks.Continuous.FirstOrder firstOrder3(
    T=coeff[2,4],
    k=coeff[1,4],
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-116,428},{-96,448}})));
  Modelica.Blocks.Continuous.FirstOrder secondOrder3(
    T=coeff[3,4],
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-80,428},{-60,448}})));
  Modelica.Blocks.Nonlinear.FixedDelay fixedDelay3(delayTime=coeff[4,4])
    annotation (Placement(transformation(extent={{-42,428},{-22,448}})));
  Modelica.Blocks.Sources.Constant iniVal3(k=coeff[5,4])
    "Initial value of the process"
    annotation (Placement(transformation(extent={{-42,396},{-22,416}})));
  Modelica.Blocks.Math.Add FinalValue3
    annotation (Placement(transformation(extent={{0,422},{20,442}})));
  Modelica.Blocks.Continuous.FirstOrder firstOrder4(
    T=coeff[2,5],
    k=coeff[1,5],
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-116,356},{-96,376}})));
  Modelica.Blocks.Continuous.FirstOrder secondOrder4(
    T=coeff[3,5],
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-80,356},{-60,376}})));
  Modelica.Blocks.Nonlinear.FixedDelay fixedDelay4(delayTime=coeff[4,5])
    annotation (Placement(transformation(extent={{-42,356},{-22,376}})));
  Modelica.Blocks.Sources.Constant iniVal4(k=coeff[5,5])
    "Initial value of the process"
    annotation (Placement(transformation(extent={{-42,324},{-22,344}})));
  Modelica.Blocks.Math.Add FinalValue4
    annotation (Placement(transformation(extent={{0,350},{20,370}})));
  Modelica.Blocks.Continuous.FirstOrder firstOrder5(
    T=coeff[2,6],
    k=coeff[1,6],
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-116,286},{-96,306}})));
  Modelica.Blocks.Continuous.FirstOrder secondOrder5(
    T=coeff[3,6],
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-80,286},{-60,306}})));
  Modelica.Blocks.Nonlinear.FixedDelay fixedDelay5(delayTime=coeff[4,6])
    annotation (Placement(transformation(extent={{-42,286},{-22,306}})));
  Modelica.Blocks.Sources.Constant iniVal5(k=coeff[5,6])
    "Initial value of the process"
    annotation (Placement(transformation(extent={{-42,254},{-22,274}})));
  Modelica.Blocks.Math.Add FinalValue5
    annotation (Placement(transformation(extent={{0,280},{20,300}})));
  Modelica.Blocks.Continuous.FirstOrder firstOrder6(
    T=coeff[2,7],
    k=coeff[1,7],
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-116,212},{-96,232}})));
  Modelica.Blocks.Continuous.FirstOrder secondOrder6(
    T=coeff[3,7],
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-80,212},{-60,232}})));
  Modelica.Blocks.Nonlinear.FixedDelay fixedDelay6(delayTime=coeff[4,7])
    annotation (Placement(transformation(extent={{-42,212},{-22,232}})));
  Modelica.Blocks.Sources.Constant iniVal6(k=coeff[5,7])
    "Initial value of the process"
    annotation (Placement(transformation(extent={{-42,180},{-22,200}})));
  Modelica.Blocks.Math.Add FinalValue6
    annotation (Placement(transformation(extent={{0,206},{20,226}})));
  Modelica.Blocks.Continuous.FirstOrder firstOrder7(
    T=coeff[2,8],
    k=coeff[1,8],
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-118,140},{-98,160}})));
  Modelica.Blocks.Continuous.FirstOrder secondOrder7(
    T=coeff[3,8],
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-82,140},{-62,160}})));
  Modelica.Blocks.Nonlinear.FixedDelay fixedDelay7(delayTime=coeff[4,8])
    annotation (Placement(transformation(extent={{-44,140},{-24,160}})));
  Modelica.Blocks.Sources.Constant iniVal7(k=coeff[5,8])
    "Initial value of the process"
    annotation (Placement(transformation(extent={{-44,108},{-24,128}})));
  Modelica.Blocks.Math.Add FinalValue7
    annotation (Placement(transformation(extent={{-2,134},{18,154}})));
  Modelica.Blocks.Continuous.FirstOrder firstOrder8(
    T=coeff[2,9],
    k=coeff[1,9],
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-120,58},{-100,78}})));
  Modelica.Blocks.Continuous.FirstOrder secondOrder8(
    T=coeff[3,9],
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-84,58},{-64,78}})));
  Modelica.Blocks.Nonlinear.FixedDelay fixedDelay8(delayTime=coeff[4,9])
    annotation (Placement(transformation(extent={{-46,58},{-26,78}})));
  Modelica.Blocks.Sources.Constant iniVal8(k=coeff[5,9])
    "Initial value of the process"
    annotation (Placement(transformation(extent={{-46,22},{-26,42}})));
  Modelica.Blocks.Math.Add FinalValue8
    annotation (Placement(transformation(extent={{-4,52},{16,72}})));
  Modelica.Blocks.Continuous.FirstOrder firstOrder9(
    T=coeff[2,10],
    k=coeff[1,10],
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-118,-24},{-98,-4}})));
  Modelica.Blocks.Continuous.FirstOrder secondOrder9(
    T=coeff[3,10],
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-84,-24},{-64,-4}})));
  Modelica.Blocks.Nonlinear.FixedDelay fixedDelay9(delayTime=coeff[4,10])
    annotation (Placement(transformation(extent={{-46,-24},{-26,-4}})));
  Modelica.Blocks.Sources.Constant iniVal9(k=coeff[5,10])
    "Initial value of the process"
    annotation (Placement(transformation(extent={{-46,-72},{-26,-52}})));
  Modelica.Blocks.Math.Add FinalValue9
    annotation (Placement(transformation(extent={{-4,-30},{16,-10}})));
  Modelica.Blocks.Continuous.FirstOrder firstOrder10(
    T=coeff[2,11],
    k=coeff[1,11],
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-120,-120},{-100,-100}})));
  Modelica.Blocks.Continuous.FirstOrder secondOrder10(
    T=coeff[3,11],
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-84,-120},{-64,-100}})));
  Modelica.Blocks.Nonlinear.FixedDelay fixedDelay10(delayTime=coeff[4,11])
    annotation (Placement(transformation(extent={{-46,-120},{-26,-100}})));
  Modelica.Blocks.Sources.Constant iniVal10(k=coeff[5,11])
    "Initial value of the process"
    annotation (Placement(transformation(extent={{-46,-160},{-26,-140}})));
  Modelica.Blocks.Math.Add FinalValue10
    annotation (Placement(transformation(extent={{-4,-126},{16,-106}})));
  Modelica.Blocks.Continuous.FirstOrder firstOrder11(
    T=coeff[2,12],
    k=coeff[1,12],
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-120,-210},{-100,-190}})));
  Modelica.Blocks.Continuous.FirstOrder secondOrder11(
    T=coeff[3,12],
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-84,-210},{-64,-190}})));
  Modelica.Blocks.Nonlinear.FixedDelay fixedDelay11(delayTime=coeff[4,12])
    annotation (Placement(transformation(extent={{-46,-210},{-26,-190}})));
  Modelica.Blocks.Sources.Constant iniVal11(k=coeff[5,12])
    "Initial value of the process"
    annotation (Placement(transformation(extent={{-46,-242},{-26,-222}})));
  Modelica.Blocks.Math.Add FinalValue11
    annotation (Placement(transformation(extent={{-4,-216},{16,-196}})));
  Modelica.Blocks.Continuous.FirstOrder firstOrder12(
    T=coeff[2,13],
    k=coeff[1,13],
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-120,-288},{-100,-268}})));
  Modelica.Blocks.Continuous.FirstOrder secondOrder12(
    T=coeff[3,13],
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-84,-288},{-64,-268}})));
  Modelica.Blocks.Nonlinear.FixedDelay fixedDelay12(delayTime=coeff[4,13])
    annotation (Placement(transformation(extent={{-46,-288},{-26,-268}})));
  Modelica.Blocks.Sources.Constant iniVal12(k=coeff[5,13])
    "Initial value of the process"
    annotation (Placement(transformation(extent={{-46,-320},{-26,-300}})));
  Modelica.Blocks.Math.Add FinalValue12
    annotation (Placement(transformation(extent={{-4,-294},{16,-274}})));
  Modelica.Blocks.Continuous.FirstOrder firstOrder13(
    T=coeff[2,14],
    k=coeff[1,14],
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-120,-364},{-100,-344}})));
  Modelica.Blocks.Continuous.FirstOrder secondOrder13(
    T=coeff[3,14],
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-84,-364},{-64,-344}})));
  Modelica.Blocks.Nonlinear.FixedDelay fixedDelay13(delayTime=coeff[4,14])
    annotation (Placement(transformation(extent={{-46,-364},{-26,-344}})));
  Modelica.Blocks.Sources.Constant iniVal13(k=coeff[5,14])
    "Initial value of the process"
    annotation (Placement(transformation(extent={{-46,-396},{-26,-376}})));
  Modelica.Blocks.Math.Add FinalValue13
    annotation (Placement(transformation(extent={{-4,-370},{16,-350}})));
  Modelica.Blocks.Continuous.FirstOrder firstOrder14(
    T=coeff[2,15],
    k=coeff[1,15],
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-118,-440},{-98,-420}})));
  Modelica.Blocks.Continuous.FirstOrder secondOrder14(
    T=coeff[3,15],
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-82,-440},{-62,-420}})));
  Modelica.Blocks.Nonlinear.FixedDelay fixedDelay14(delayTime=coeff[4,15])
    annotation (Placement(transformation(extent={{-44,-440},{-24,-420}})));
  Modelica.Blocks.Sources.Constant iniVal14(k=coeff[5,15])
    "Initial value of the process"
    annotation (Placement(transformation(extent={{-44,-472},{-24,-452}})));
  Modelica.Blocks.Math.Add FinalValue14
    annotation (Placement(transformation(extent={{-2,-446},{18,-426}})));
  Modelica.Blocks.Continuous.FirstOrder firstOrder15(
    T=coeff[2,16],
    k=coeff[1,16],
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-118,-512},{-98,-492}})));
  Modelica.Blocks.Continuous.FirstOrder secondOrder15(
    T=coeff[3,16],
    initType=Modelica.Blocks.Types.Init.SteadyState,
    y_start=0)
    annotation (Placement(transformation(extent={{-82,-512},{-62,-492}})));
  Modelica.Blocks.Nonlinear.FixedDelay fixedDelay15(delayTime=coeff[4,16])
    annotation (Placement(transformation(extent={{-44,-512},{-24,-492}})));
  Modelica.Blocks.Sources.Constant iniVal15(k=coeff[5,16])
    "Initial value of the process"
    annotation (Placement(transformation(extent={{-44,-544},{-24,-524}})));
  Modelica.Blocks.Math.Add FinalValue15
    annotation (Placement(transformation(extent={{-2,-518},{18,-498}})));
  Buildings.Utilities.Math.Average ave(nin=4)
    annotation (Placement(transformation(extent={{216,528},{238,550}})));
  Modelica.Blocks.Routing.Multiplex4 multiplex4_1
    annotation (Placement(transformation(extent={{158,528},{178,548}})));
  Modelica.Blocks.Routing.Multiplex4 multiplex4_2
    annotation (Placement(transformation(extent={{166,236},{186,256}})));
  Modelica.Blocks.Routing.Multiplex4 multiplex4_3
    annotation (Placement(transformation(extent={{168,-78},{188,-58}})));
  Modelica.Blocks.Routing.Multiplex4 multiplex4_4
    annotation (Placement(transformation(extent={{178,-408},{198,-388}})));
  Modelica.Blocks.Routing.Multiplex4 multiplex4_5
    annotation (Placement(transformation(extent={{338,-4},{358,16}})));
  Buildings.Utilities.Math.Average ave1(nin=4)
    annotation (Placement(transformation(extent={{224,236},{246,258}})));
  Buildings.Utilities.Math.Average ave2(nin=4)
    annotation (Placement(transformation(extent={{232,-78},{254,-56}})));
  Buildings.Utilities.Math.Average ave3(nin=4)
    annotation (Placement(transformation(extent={{238,-410},{260,-388}})));
  Buildings.Utilities.Math.Average ave4(nin=4)
    annotation (Placement(transformation(extent={{406,-6},{428,16}})));
  Modelica.Blocks.Interfaces.RealInput u annotation (Placement(transformation(extent={{-636,4},{
            -596,44}}),  iconTransformation(extent={{-682,-42},{-596,44}})));
  Modelica.Blocks.Interfaces.RealOutput y annotation (Placement(transformation(extent={{548,-4},
            {568,16}}), iconTransformation(extent={{540,-44},{630,46}})));
equation
  connect(firstOrder.y, secondOrder.u)
    annotation (Line(points={{-95,650},{-82,650}}, color={0,0,127}));
  connect(secondOrder.y, fixedDelay.u)
    annotation (Line(points={{-59,650},{-44,650}}, color={0,0,127}));
  connect(fixedDelay.y, FinalValue.u1)
    annotation (Line(points={{-21,650},{-2,650}}, color={0,0,127}));
  connect(iniVal.y, FinalValue.u2)
    annotation (Line(points={{-21,618},{-21,638},{-2,638}}, color={0,0,127}));
  connect(firstOrder1.y, secondOrder1.u)
    annotation (Line(points={{-95,576},{-82,576}}, color={0,0,127}));
  connect(secondOrder1.y, fixedDelay1.u)
    annotation (Line(points={{-59,576},{-44,576}}, color={0,0,127}));
  connect(fixedDelay1.y, FinalValue1.u1)
    annotation (Line(points={{-21,576},{-2,576}}, color={0,0,127}));
  connect(iniVal1.y, FinalValue1.u2)
    annotation (Line(points={{-21,544},{-21,564},{-2,564}}, color={0,0,127}));
  connect(firstOrder2.y, secondOrder2.u)
    annotation (Line(points={{-95,506},{-82,506}}, color={0,0,127}));
  connect(secondOrder2.y, fixedDelay2.u)
    annotation (Line(points={{-59,506},{-44,506}}, color={0,0,127}));
  connect(fixedDelay2.y, FinalValue2.u1)
    annotation (Line(points={{-21,506},{-2,506}}, color={0,0,127}));
  connect(iniVal2.y, FinalValue2.u2)
    annotation (Line(points={{-21,474},{-21,494},{-2,494}}, color={0,0,127}));
  connect(firstOrder3.y, secondOrder3.u)
    annotation (Line(points={{-95,438},{-82,438}}, color={0,0,127}));
  connect(secondOrder3.y, fixedDelay3.u)
    annotation (Line(points={{-59,438},{-44,438}}, color={0,0,127}));
  connect(fixedDelay3.y, FinalValue3.u1)
    annotation (Line(points={{-21,438},{-2,438}}, color={0,0,127}));
  connect(iniVal3.y, FinalValue3.u2)
    annotation (Line(points={{-21,406},{-21,426},{-2,426}}, color={0,0,127}));
  connect(firstOrder4.y, secondOrder4.u)
    annotation (Line(points={{-95,366},{-82,366}}, color={0,0,127}));
  connect(secondOrder4.y, fixedDelay4.u)
    annotation (Line(points={{-59,366},{-44,366}}, color={0,0,127}));
  connect(fixedDelay4.y, FinalValue4.u1)
    annotation (Line(points={{-21,366},{-2,366}}, color={0,0,127}));
  connect(iniVal4.y, FinalValue4.u2)
    annotation (Line(points={{-21,334},{-21,354},{-2,354}}, color={0,0,127}));
  connect(firstOrder5.y, secondOrder5.u)
    annotation (Line(points={{-95,296},{-82,296}}, color={0,0,127}));
  connect(secondOrder5.y, fixedDelay5.u)
    annotation (Line(points={{-59,296},{-44,296}}, color={0,0,127}));
  connect(fixedDelay5.y, FinalValue5.u1)
    annotation (Line(points={{-21,296},{-2,296}}, color={0,0,127}));
  connect(iniVal5.y, FinalValue5.u2)
    annotation (Line(points={{-21,264},{-21,284},{-2,284}}, color={0,0,127}));
  connect(firstOrder6.y, secondOrder6.u)
    annotation (Line(points={{-95,222},{-82,222}}, color={0,0,127}));
  connect(secondOrder6.y, fixedDelay6.u)
    annotation (Line(points={{-59,222},{-44,222}}, color={0,0,127}));
  connect(fixedDelay6.y, FinalValue6.u1)
    annotation (Line(points={{-21,222},{-2,222}}, color={0,0,127}));
  connect(iniVal6.y, FinalValue6.u2)
    annotation (Line(points={{-21,190},{-21,210},{-2,210}}, color={0,0,127}));
  connect(firstOrder7.y, secondOrder7.u)
    annotation (Line(points={{-97,150},{-84,150}}, color={0,0,127}));
  connect(secondOrder7.y, fixedDelay7.u)
    annotation (Line(points={{-61,150},{-46,150}}, color={0,0,127}));
  connect(fixedDelay7.y, FinalValue7.u1)
    annotation (Line(points={{-23,150},{-4,150}}, color={0,0,127}));
  connect(iniVal7.y, FinalValue7.u2)
    annotation (Line(points={{-23,118},{-23,138},{-4,138}}, color={0,0,127}));
  connect(firstOrder8.y, secondOrder8.u)
    annotation (Line(points={{-99,68},{-86,68}}, color={0,0,127}));
  connect(secondOrder8.y, fixedDelay8.u)
    annotation (Line(points={{-63,68},{-48,68}}, color={0,0,127}));
  connect(fixedDelay8.y, FinalValue8.u1)
    annotation (Line(points={{-25,68},{-6,68}}, color={0,0,127}));
  connect(iniVal8.y, FinalValue8.u2)
    annotation (Line(points={{-25,32},{-25,56},{-6,56}}, color={0,0,127}));
  connect(firstOrder9.y, secondOrder9.u)
    annotation (Line(points={{-97,-14},{-86,-14}},
                                               color={0,0,127}));
  connect(secondOrder9.y, fixedDelay9.u)
    annotation (Line(points={{-63,-14},{-48,-14}},
                                               color={0,0,127}));
  connect(fixedDelay9.y, FinalValue9.u1)
    annotation (Line(points={{-25,-14},{-6,-14}},
                                              color={0,0,127}));
  connect(iniVal9.y, FinalValue9.u2)
    annotation (Line(points={{-25,-62},{-25,-26},{-6,-26}},
                                                          color={0,0,127}));
  connect(firstOrder10.y, secondOrder10.u)
    annotation (Line(points={{-99,-110},{-86,-110}}, color={0,0,127}));
  connect(secondOrder10.y, fixedDelay10.u)
    annotation (Line(points={{-63,-110},{-48,-110}}, color={0,0,127}));
  connect(fixedDelay10.y, FinalValue10.u1)
    annotation (Line(points={{-25,-110},{-6,-110}}, color={0,0,127}));
  connect(iniVal10.y, FinalValue10.u2) annotation (Line(points={{-25,-150},{-25,
          -122},{-6,-122}}, color={0,0,127}));
  connect(firstOrder11.y, secondOrder11.u)
    annotation (Line(points={{-99,-200},{-86,-200}}, color={0,0,127}));
  connect(secondOrder11.y, fixedDelay11.u)
    annotation (Line(points={{-63,-200},{-48,-200}}, color={0,0,127}));
  connect(fixedDelay11.y, FinalValue11.u1)
    annotation (Line(points={{-25,-200},{-6,-200}}, color={0,0,127}));
  connect(iniVal11.y, FinalValue11.u2) annotation (Line(points={{-25,-232},{-25,-212},{-6,-212}},
                            color={0,0,127}));
  connect(firstOrder12.y, secondOrder12.u)
    annotation (Line(points={{-99,-278},{-86,-278}}, color={0,0,127}));
  connect(secondOrder12.y, fixedDelay12.u)
    annotation (Line(points={{-63,-278},{-48,-278}}, color={0,0,127}));
  connect(fixedDelay12.y, FinalValue12.u1)
    annotation (Line(points={{-25,-278},{-6,-278}}, color={0,0,127}));
  connect(iniVal12.y, FinalValue12.u2) annotation (Line(points={{-25,-310},{-25,-290},{-6,-290}},
                            color={0,0,127}));
  connect(firstOrder13.y, secondOrder13.u)
    annotation (Line(points={{-99,-354},{-86,-354}}, color={0,0,127}));
  connect(secondOrder13.y, fixedDelay13.u)
    annotation (Line(points={{-63,-354},{-48,-354}}, color={0,0,127}));
  connect(fixedDelay13.y, FinalValue13.u1)
    annotation (Line(points={{-25,-354},{-6,-354}}, color={0,0,127}));
  connect(iniVal13.y, FinalValue13.u2) annotation (Line(points={{-25,-386},{-25,
          -366},{-6,-366}}, color={0,0,127}));
  connect(firstOrder14.y, secondOrder14.u)
    annotation (Line(points={{-97,-430},{-84,-430}}, color={0,0,127}));
  connect(secondOrder14.y, fixedDelay14.u)
    annotation (Line(points={{-61,-430},{-46,-430}}, color={0,0,127}));
  connect(fixedDelay14.y, FinalValue14.u1)
    annotation (Line(points={{-23,-430},{-4,-430}}, color={0,0,127}));
  connect(iniVal14.y, FinalValue14.u2) annotation (Line(points={{-23,-462},{-23,-442},{-4,-442}},
                            color={0,0,127}));
  connect(firstOrder15.y, secondOrder15.u)
    annotation (Line(points={{-97,-502},{-84,-502}}, color={0,0,127}));
  connect(secondOrder15.y, fixedDelay15.u)
    annotation (Line(points={{-61,-502},{-46,-502}}, color={0,0,127}));
  connect(fixedDelay15.y, FinalValue15.u1)
    annotation (Line(points={{-23,-502},{-4,-502}}, color={0,0,127}));
  connect(iniVal15.y, FinalValue15.u2) annotation (Line(points={{-23,-534},{-23,-514},{-4,-514}},
                            color={0,0,127}));
  connect(FinalValue.y, multiplex4_1.u1[1]) annotation (Line(points={{21,644},{128,644},{128,546},
          {156,546},{156,547}}, color={0,0,127}));
  connect(FinalValue1.y, multiplex4_1.u2[1]) annotation (Line(points={{21,570},{128,570},{128,541},
          {156,541}}, color={0,0,127}));
  connect(FinalValue2.y, multiplex4_1.u3[1]) annotation (Line(points={{21,500},{128,500},{128,535},
          {156,535}}, color={0,0,127}));
  connect(FinalValue3.y, multiplex4_1.u4[1]) annotation (Line(points={{21,432},{128,432},{128,529},
          {156,529}}, color={0,0,127}));
  connect(FinalValue4.y, multiplex4_2.u1[1])
    annotation (Line(points={{21,360},{94,360},{94,255},{164,255}}, color={0,0,127}));
  connect(FinalValue5.y, multiplex4_2.u2[1]) annotation (Line(points={{21,290},{94,290},{94,249},
          {164,249}},                   color={0,0,127}));
  connect(FinalValue6.y, multiplex4_2.u3[1]) annotation (Line(points={{21,216},{96,216},{96,243},
          {164,243}},                   color={0,0,127}));
  connect(FinalValue7.y, multiplex4_2.u4[1]) annotation (Line(points={{19,144},{96,144},{96,237},
          {164,237}},                   color={0,0,127}));
  connect(FinalValue8.y, multiplex4_3.u1[1]) annotation (Line(points={{17,62},{94,62},{94,-58},{
          166,-58},{166,-59}}, color={0,0,127}));
  connect(FinalValue9.y, multiplex4_3.u2[1])
    annotation (Line(points={{17,-20},{94,-20},{94,-65},{166,-65}}, color={0,0,127}));
  connect(FinalValue10.y, multiplex4_3.u3[1]) annotation (Line(points={{17,-116},{88,-116},{88,-71},
          {166,-71}}, color={0,0,127}));
  connect(FinalValue11.y, multiplex4_3.u4[1]) annotation (Line(points={{17,-206},{90,-206},{90,-77},
          {166,-77}},                    color={0,0,127}));
  connect(FinalValue12.y, multiplex4_4.u1[1]) annotation (Line(points={{17,-284},{96,-284},{96,-389},
          {176,-389}}, color={0,0,127}));
  connect(FinalValue13.y, multiplex4_4.u2[1]) annotation (Line(points={{17,-360},{98,-360},{98,-395},
          {176,-395}}, color={0,0,127}));
  connect(FinalValue14.y, multiplex4_4.u3[1]) annotation (Line(points={{19,-436},{96,-436},{96,-401},
          {176,-401}}, color={0,0,127}));
  connect(FinalValue15.y, multiplex4_4.u4[1]) annotation (Line(points={{19,-508},{98,-508},{98,-407},
          {176,-407}}, color={0,0,127}));
  connect(multiplex4_1.y, ave.u) annotation (Line(points={{179,538},{198,538},{198,539},{213.8,539}},
        color={0,0,127}));
  connect(multiplex4_2.y, ave1.u) annotation (Line(points={{187,246},{204,246},{204,247},{221.8,
          247}}, color={0,0,127}));
  connect(multiplex4_3.y, ave2.u) annotation (Line(points={{189,-68},{208.5,-68},{208.5,-67},{229.8,
          -67}}, color={0,0,127}));
  connect(multiplex4_4.y, ave3.u) annotation (Line(points={{199,-398},{218,-398},{218,-399},{235.8,
          -399}}, color={0,0,127}));
  connect(ave.y, multiplex4_5.u1[1]) annotation (Line(points={{239.1,539},{298,539},{298,15},{336,
          15}}, color={0,0,127}));
  connect(ave1.y, multiplex4_5.u2[1]) annotation (Line(points={{247.1,247},{300,247},{300,9},{336,
          9}}, color={0,0,127}));
  connect(ave2.y, multiplex4_5.u3[1]) annotation (Line(points={{255.1,-67},{298,-67},{298,3},{336,
          3}},  color={0,0,127}));
  connect(ave3.y, multiplex4_5.u4[1]) annotation (Line(points={{261.1,-399},{298,-399},{298,-2},
          {336,-2},{336,-3}}, color={0,0,127}));
  connect(multiplex4_5.y, ave4.u)
    annotation (Line(points={{359,6},{380,6},{380,5},{403.8,5}}, color={0,0,127}));
  connect(ave4.y, y) annotation (Line(points={{429.1,5},{489.55,5},{489.55,6},{558,6}}, color={0,
          0,127}));
  connect(y, y) annotation (Line(points={{558,6},{558,6}}, color={0,0,127}));
  connect(u, firstOrder.u) annotation (Line(points={{-616,24},{-216,24},{-216,650},{-118,650}},
        color={0,0,127}));
  connect(u, firstOrder1.u) annotation (Line(points={{-616,24},{-214,24},{-214,576},{-118,576}},
        color={0,0,127}));
  connect(u, firstOrder2.u) annotation (Line(points={{-616,24},{-214,24},{-214,506},{-118,506}},
        color={0,0,127}));
  connect(u, firstOrder3.u) annotation (Line(points={{-616,24},{-214,24},{-214,438},{-118,438}},
        color={0,0,127}));
  connect(u, firstOrder4.u) annotation (Line(points={{-616,24},{-214,24},{-214,366},{-118,366}},
        color={0,0,127}));
  connect(u, firstOrder5.u) annotation (Line(points={{-616,24},{-214,24},{-214,296},{-118,296}},
        color={0,0,127}));
  connect(u, firstOrder6.u) annotation (Line(points={{-616,24},{-216,24},{-216,222},{-118,222}},
        color={0,0,127}));
  connect(u, firstOrder7.u) annotation (Line(points={{-616,24},{-216,24},{-216,150},{-120,150}},
        color={0,0,127}));
  connect(u, firstOrder8.u) annotation (Line(points={{-616,24},{-216,24},{-216,68},{-122,68}},
        color={0,0,127}));
  connect(u, firstOrder9.u) annotation (Line(points={{-616,24},{-216,24},{-216,-14},{-120,-14}},
        color={0,0,127}));
  connect(u, firstOrder10.u) annotation (Line(points={{-616,24},{-212,24},{-212,-110},{-122,-110}},
        color={0,0,127}));
  connect(u, firstOrder11.u) annotation (Line(points={{-616,24},{-216,24},{-216,-200},{-122,-200}},
        color={0,0,127}));
  connect(u, firstOrder12.u) annotation (Line(points={{-616,24},{-216,24},{-216,-278},{-122,-278}},
        color={0,0,127}));
  connect(u, firstOrder13.u) annotation (Line(points={{-616,24},{-214,24},{-214,-354},{-122,-354}},
        color={0,0,127}));
  connect(u, firstOrder14.u) annotation (Line(points={{-616,24},{-212,24},{-212,-430},{-120,-430}},
        color={0,0,127}));
  connect(u, firstOrder15.u) annotation (Line(points={{-616,24},{-214,24},{-214,-502},{-120,-502}},
        color={0,0,127}));
  annotation (Diagram(coordinateSystem(extent={{-600,-560},{540,680}})), Icon(coordinateSystem(
          extent={{-600,-560},{540,680}}), graphics={Rectangle(
          extent={{-598,680},{542,-556}},
          lineColor={0,0,0},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid), Text(
          extent={{-468,844},{386,692}},
          lineColor={0,0,255},
          lineThickness=0.5,
          fillColor={255,255,255},
          fillPattern=FillPattern.None,
          textString="%name")}));
end TransferFuns;
