# -*- coding: utf-8 -*-
"""
Created on Wed Sep 19 15:04:54 2018

@author: OliverWang
"""

import os
from buildingspy.io.outputfile import Reader
import numpy as np
import scipy.io as sio
import shutil
import pandas as pd
#import winsound
import timeit
import matplotlib.pyplot as plt


def translate(model,packagePath,outputPath):
    import os
    from buildingspy.simulate.Simulator import Simulator
    
    # Check input files: if before translation, input files exsit, then delete them.
    fileList=[outputPath+'/dymosim.exe',outputPath+'/dsin.txt',outputPath+'/dsres.mat',
              outputPath+'/../Resources/InputFiles/bouTemInput.txt',
              outputPath+'/../Resources/InputFiles/PLCUncInput.txt',
              outputPath+'/../Resources/InputFiles/risTimInput.txt',
              outputPath+'/../Resources/InputFiles/TransferFunInput_validation.txt',
              outputPath+'/../Resources/InputFiles/PIDInput.txt',
              outputPath+'/../Resources/InputFiles/allTransFun.mat',
              outputPath+'/../Resources/InputFiles/TransferFunInput_validationAve.txt']
    for fil in fileList:
        if os.path.exists(fil):
            os.remove(fil)
    ## Translate model to get dymosim.exe and dsin.txt
    s = Simulator(model, "dymola")
    s.setOutputDirectory(outputPath)
    s.setPackagePath(packagePath)
    s.showGUI(show=False)
    s.printModelAndTime()
    # This attribute does not exist in the official buildingpy library
    s.chooseCurWorDir(curWorDir= True)
    s.translate()
    if outputPath != packagePath:
        s._copyResultFiles(packagePath)
    return s


############################################################
#Main script
if __name__ == "__main__":   
    
    repoPath=os.getcwd()
    resultPath = repoPath+'/Performance Validation'
    buildingsPath = repoPath+"/Buildings"
    
    ScenarioNum = 1
    finish = 2
    targetSensor = 13

    ## Translate model
#    model = 'ModelicaModels.System.PIDPerformanceValidation'
#    s = translate(model,repoPath+"\ModelicaModels",buildingsPath)
#    ##Rename the translated models and input file respectively for PID and PI control
#    os.chdir(buildingsPath)
#    os.rename('dymosim.exe','dymosimPID.exe')
#    os.rename('dsin.txt','dsinPID.txt')
    
    # Simulation settings
    startTime = 0
    endTime = 5000
    solver = 4
    Increment = 5
    tolerance = 0.000001
    for fil in ['dsinPID.txt']:
        f=open(buildingsPath+'/'+fil,'r') 
        lines=f.readlines()
        f.close()
        # Set user-defined solver: Dassl
        lines[9] = "    %d                   # StartTime    Time at which integration starts\n" %(startTime)
        lines[11]= "    %d                   # StopTime     Time at which integration stops\n" %(endTime)
        lines[12]= "    %d                   # Increment    Communication step size, if > 0\n"%(Increment)
        lines[14]= "    %f             # Tolerance    Relative precision of signals for\n" %(tolerance)
        lines[18] = "      %d                    # Algorithm    Integration algorithm as integer (1...28)\n" %(solver)
        f=open(buildingsPath+'/'+fil,'w')
        for k in range(len(lines)):
            f.writelines(lines[k])
        f.close()
        
    PID = pd.read_csv(os.path.join(repoPath,'PID_final.csv')).iloc[:,1:].values
    fig, ax = plt.subplots(1,1)
     
    for i in np.arange(finish,finish+ScenarioNum,1):
        for j in np.arange(targetSensor-1,targetSensor,1):
            
            f=open(repoPath+"/Resources/InputFiles/PIDInput.txt","w+")
            f.write('Kp=%f;\n'%(PID[0,j]))
            f.write('Ti=%f;\n'%(PID[1,j]))
            f.write('Td=%f;\n'%(PID[2,j]))
            f.close()
            
            # read transfer function parameters
            parameters = pd.read_csv(os.path.join(repoPath,'Results_dymola','Summary','Transfer Function_Scenario-'+str(i+1)+'.csv')).iloc[:,1:].values

            sio.savemat(repoPath+'\Resources\InputFiles\AllTransferFunctions.mat',{'allTransFuns':parameters})
            
            #Local tempearatures
            f=open(repoPath+"/Resources/InputFiles/TransferFunInput_validation.txt","w+")
            f.write('K=%f;\n'%(parameters[0,j]))
            f.write('T1=%f;\n'%(parameters[1,j]))
            f.write('T2=%f;\n'%(parameters[2,j]))
            f.write('D=%f;\n'%(parameters[3,j]))
            f.write('IniVal=%f;\n'%(parameters[4,j]))
            f.close()
            
            #Ideal average temperature
            f=open(repoPath+"/Resources/InputFiles/TransferFunInput_validationAve.txt","w+")
            f.write('KAve=%f;\n'%(parameters[0,16]))
            f.write('T1Ave=%f;\n'%(parameters[1,16]))
            f.write('T2Ave=%f;\n'%(parameters[2,16]))
            f.write('DAve=%f;\n'%(parameters[3,16]))
            f.write('IniValAve=%f;\n'%(parameters[4,16]))
            f.close()
            
            ## Run modelica model
            os.chdir(buildingsPath)
            if PID[2,j] == 0:
                os.system(buildingsPath+'\dymosimPI.exe '+ buildingsPath+'\dsinPI.txt')
            else:
                os.system(buildingsPath+'\dymosimPID.exe '+ buildingsPath+'\dsinPID.txt')    
        
            #Read temperature data 
            ResultFile = os.path.join(buildingsPath,"dsres.mat")
            r = Reader(ResultFile,"dymola")
            
            (time,LocAvetemp) = r.values('transferFuns.y')
            (time,IdealAvetemp) = r.values('FinalValue1.y')
            (time,Loctemp) = r.values('FinalValue.y')
            (time,setpoint) = r.values('Setpoint.y')
            
            LocTempDF = pd.DataFrame(Loctemp,columns=['LocTemp'])
            SetpointDF = pd.DataFrame(setpoint,columns=['SetPoint'])
            timeDF = pd.DataFrame(time,columns=['Time'])
            Data1 = pd.concat([timeDF,SetpointDF,LocTempDF],axis=1)
            plotdata = Data1.drop_duplicates('Time')
            plotdata.index = range(np.size(plotdata,axis=0))
            plotdata.to_csv(resultPath+'/TargetSensor.csv')
            
            if i == finish+ScenarioNum-1:
                ax.plot(plotdata[(plotdata.Time>=1000)]["Time"].values-1000,plotdata[(plotdata.Time>=1000)]["LocTemp"].values-273.15,color='0.75', label = "LocTemp")
                ax.plot(plotdata[(plotdata.Time>=1000)]["Time"].values-1000,plotdata[(plotdata.Time>=1000)]["SetPoint"].values-273.15,'r',label = "Setpoint")
            else:
                ax.plot(plotdata[(plotdata.Time>=1000)]["Time"].values-1000,plotdata[(plotdata.Time>=1000)]["LocTemp"].values-273.15,color='0.75')
            print i+1
            
            ax.set_xlabel('Time/s',fontsize = 12)
            ax.legend(fontsize = 12,loc=1)
            ax.grid(color='0.75',linestyle='--',linewidth=0.3)            
         
        fig.savefig(repoPath+'/Validation_TargetSensor.svg',format='svg',dpi=1000)  
            
            
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    