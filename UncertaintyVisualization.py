# -*- coding: utf-8 -*-
"""
Created on Fri Apr 20 10:29:17 2018

@author: Administrator
"""

# -*- coding: utf-8 -*-
"""
Created on Sun Sep 10 13:34:12 2017

@author: Administrator
"""

import scipy.io as sio, scipy.stats as st
import numpy as np
import matplotlib.pyplot as plt
import os
import re
import pandas as pd
#from math import sqrt
#from math import pi



path = os.getcwd()
RawDataPath = os.path.join(path,'Results_dymola','Summary')
#
TotalNumber = 100
SensorNumber= 17

###########################################################
# Generate zeros array for every variables
K = np.zeros((TotalNumber,SensorNumber))
T1 = np.zeros((TotalNumber,SensorNumber))
T2 = np.zeros((TotalNumber,SensorNumber))
D = np.zeros((TotalNumber,SensorNumber))
IniVal = np.zeros((TotalNumber,SensorNumber))
TF_final = np.zeros((5,SensorNumber))
#
#interval = 0.1 #used to define the range of limitation
bins = 15  #used to define the amount of bins in histogram
#
# Read data from csv files and save it in corresponding arrays
for i in range(TotalNumber):
    f = pd.read_csv(RawDataPath+'/Transfer Function_Scenario-'+str(i+1)+'.csv').iloc[:,1:SensorNumber+1].values
    K[i,:] = f[0,:]
    T1[i,:] = f[1,:]
    T2[i,:] = f[2,:]
    D[i,:] = f[3,:]
    IniVal[i,:] = f[4,:]

for k in range(SensorNumber):
    DF = {'K':K[:,k],'T1':T1[:,k],'T2':T2[:,k],'D':D[:,k],'IniVal':IniVal[:,k]}
    parameter = pd.DataFrame(data=DF,columns=['K','T1','T2','D','IniVal'])
    
    ftf, ax = plt.subplots(1,5,figsize=(20,6))
    ftf.text(0.09,0.5,'Frequency', ha='center', va='center', rotation='vertical',fontsize=15)
    
    value_mostlikely = []
    lowerLim=[]
    upperLim=[]
    label = ['K','T1','T2','D','IniVal']
    for i in range(5):
        ax1 = ax[i]
        weights = np.ones_like(parameter.iloc[:,i])/np.size(parameter.iloc[:,i])
        fr,binEdge = np.histogram(parameter.iloc[:,i],bins=bins,weights = weights)
        max_index = np.argmax(fr)
        value_mostlikely.append(0.5*(binEdge[max_index]+binEdge[max_index+1]))
        lowerLim.append(np.percentile(parameter.iloc[:,i],5))
        upperLim.append(np.percentile(parameter.iloc[:,i],95))
        index = []
        for j in range(len(fr)):
            mean=0.5*(binEdge[j]+binEdge[j+1])
            if mean > lowerLim[i] and mean < upperLim[i]:
                index.append(j)
        probability=round(sum(fr[index]),3)
        barlist = ax1.bar(binEdge[0:len(fr)],fr,width=binEdge[1]-binEdge[0],edgecolor='black',linewidth=1,color='0.75')
        barlist[index[0]].set_label('5-95th Percentile')
        for num in index:
            barlist[num].set_color('mediumseagreen')
            barlist[num].set_edgecolor('k')
            barlist[num].set_linewidth('1')
        
        ax1.set_xlabel(label[i],fontsize=15)
        ax1.legend(loc=0,fontsize=12)
    
    ftf.savefig(path+'/HistogramTF'+str(k+1)+'.svg', format='svg', dpi=1000)
    TF_final[:,k] = value_mostlikely
    
TF_finalDF = pd.DataFrame(TF_final,columns=['LocTemp1','LocTemp2','LocTemp3','LocTemp4',
                                                      'LocTemp5','LocTemp6','LocTemp7','LocTemp8',
                                                      'LocTemp9','LocTemp10','LocTemp11','LocTemp12',
                                                      'LocTemp13','LocTemp14','LocTemp15','LocTemp16','AveTemp'])
TF_finalDF.to_csv(path+'/TF_final.csv')
##############################################################################################
# Generate zeros array for every variables
Kp = np.zeros((TotalNumber,SensorNumber))
Ti = np.zeros((TotalNumber,SensorNumber))
Td = np.zeros((TotalNumber,SensorNumber))
PID_final = np.zeros((3,SensorNumber))

# Read data from csv files and save it in corresponding arrays
for i in range(TotalNumber):
    f = pd.read_csv(RawDataPath+'/PID_Scenario-'+str(i+1)+'.csv').iloc[:,1:SensorNumber+1].values
    Kp[i,:] = f[0,:]
    Ti[i,:] = f[1,:]
    Td[i,:] = f[2,:]

#Kp1 = Kp.reshape((np.size(Kp)),1)
#Ti1 = Ti.reshape((np.size(Ti)),1)
#Td1 = Td.reshape((np.size(Td)),1)
for k in range(SensorNumber):
    DF = {'Kp':Kp[:,k],'Ti':Ti[:,k],'Td':Td[:,k]}
    parameter = pd.DataFrame(data = DF)
    parameter = parameter[['Kp','Ti','Td']]
    parameter0 = parameter[(parameter.Td==0)]
    parameter1 = parameter[(parameter.Td!=0)]
    
    ## Histogram for PID parameters all-in
    fdp, ax = plt.subplots(1,3,figsize=(14,6))
    fdp.text(0.08, 0.5, 'Frequency', ha='center', va='center', rotation='vertical',fontsize=15)
    
    value_mostlikely=[]
    value_median = []
    lowerLim=[]
    upperLim=[]
    label = ['Kp','Ti','Td']
    for i in range(3):
        ax1 = ax[i]
        weights = np.ones_like(parameter.iloc[:,i])/np.size(parameter.iloc[:,i])
        fr,binEdge = np.histogram(parameter.iloc[:,i],bins=bins,weights = weights)
        max_index = np.argmax(fr)
        value_mostlikely.append(0.5*(binEdge[max_index]+binEdge[max_index+1]))
        value_median.append(np.percentile(parameter.iloc[:,i],50))
        lowerLim.append(np.percentile(parameter.iloc[:,i],5))
        upperLim.append(np.percentile(parameter.iloc[:,i],95))
        index = []
        for j in range(len(fr)):
            mean=0.5*(binEdge[j]+binEdge[j+1])
            if mean > lowerLim[i] and mean < upperLim[i]:
                index.append(j)
        probability=round(sum(fr[index]),3)
        barlist = ax1.bar(binEdge[0:len(fr)],fr,width=binEdge[1]-binEdge[0],edgecolor='black',linewidth=1,color='0.75')
        barlist[index[0]].set_label('5-95th Percentile')
        for num in index:
            barlist[num].set_color('mediumseagreen')
            barlist[num].set_edgecolor('k')
            barlist[num].set_linewidth('1')
        
        ax1.set_xlabel(label[i],fontsize=15)
        ax1.legend(loc=0,fontsize=12)
        if i == 0:
            ax1.annotate('Kp='+str(round(value_mostlikely[i],2)),xy=(value_mostlikely[i],0.85*np.max(fr)),xytext=(0.8*np.average(binEdge),0.5*np.max(fr)),arrowprops=dict(arrowstyle='->'),fontsize=15,weight='demibold')
        elif i == 1:
            ax1.annotate('Ti='+str(round(value_mostlikely[i],2)),xy=(value_mostlikely[i],0.85*np.max(fr)),xytext=(0.8*np.average(binEdge),0.5*np.max(fr)),arrowprops=dict(arrowstyle='->'),fontsize=15,weight='demibold')
        elif  i == 2:
            ax1.annotate('Td='+str(round(value_mostlikely[i],2)),xy=(value_mostlikely[i],0.85*np.max(fr)),xytext=(0.8*np.average(binEdge),0.5*np.max(fr)),arrowprops=dict(arrowstyle='->'),fontsize=15,weight='demibold')
    
#    f = open(RawDataPath+'/Most_likely_valuePID_one.txt','w+')
#    f.write('Kp=%f;\n'%(value_mostlikely[0]))
#    f.write('Ti=%f;\n'%(value_mostlikely[1]))
#    f.write('Td=%f;\n'%(value_mostlikely[2]))
#    f.close()
    fdp.savefig(path+'/HistogramPID'+str(k+1)+'.svg', format='svg', dpi=1000)
    PID_final[:,k] = value_mostlikely
PID_finalDF = pd.DataFrame(PID_final,columns=['LocTemp1','LocTemp2','LocTemp3','LocTemp4',
                                                      'LocTemp5','LocTemp6','LocTemp7','LocTemp8',
                                                      'LocTemp9','LocTemp10','LocTemp11','LocTemp12',
                                                      'LocTemp13','LocTemp14','LocTemp15','LocTemp16','AveTemp'])
PID_finalDF.to_csv(path+'/PID_final.csv')
#    


########################################################    
### Histogram for PID parameters_ only those whose D != 0
#fdp, ax = plt.subplots(1,3,figsize=(14,7))
#fdp.text(0.08, 0.5, 'Frequency', ha='center', va='center', rotation='vertical',fontsize=15)
#
#value_mostlikely=[]
#lowerLim=[]
#upperLim=[]
#label = ['Kp','Ti','Td']
#for i in range(3):
#    ax1 = ax[i]
#    weights = np.ones_like(parameter1.iloc[:,i])/np.size(parameter1.iloc[:,i])
#    fr,binEdge = np.histogram(parameter1.iloc[:,i],bins=bins,weights = weights)
#    max_index = np.argmax(fr)
#    value_mostlikely.append(0.5*(binEdge[max_index]+binEdge[max_index+1]))
#    lowerLim.append(np.percentile(parameter1.iloc[:,i],5))
#    upperLim.append(np.percentile(parameter1.iloc[:,i],95))
#    index = []
#    for j in range(len(fr)):
#        mean=0.5*(binEdge[j]+binEdge[j+1])
#        if mean > lowerLim[i] and mean < upperLim[i]:
#            index.append(j)
#    probability=round(sum(fr[index]),3)
#    barlist = ax1.bar(binEdge[0:len(fr)],fr,width=binEdge[1]-binEdge[0],edgecolor='black',linewidth=1,color='0.75')
#    barlist[index[0]].set_label('5-95th Percentile')
#    for num in index:
#        barlist[num].set_color('mediumseagreen')
#        barlist[num].set_edgecolor('k')
#        barlist[num].set_linewidth('1')
#    
#    ax1.set_xlabel(label[i],fontsize=15)
#    ax1.legend(loc=0,fontsize=12)
#    if i == 0:
#        ax1.annotate('Kp='+str(round(value_mostlikely[i],3)),xy=(value_mostlikely[i],0.065),xytext=(-1.5,0.047),arrowprops=dict(arrowstyle='->'),fontsize=15,weight='demibold')
#    elif i == 1:
#        ax1.annotate('Ti='+str(round(value_mostlikely[i],3)),xy=(value_mostlikely[i]-1,0.071),xytext=(82,0.051),arrowprops=dict(arrowstyle='->'),fontsize=15,weight='demibold')
#    elif  i == 2:
#        ax1.annotate('Td='+str(round(value_mostlikely[i],3)),xy=(value_mostlikely[i]-0.2,0.055),xytext=(20,0.041),arrowprops=dict(arrowstyle='->'),fontsize=15,weight='demibold')
#
#f = open(RawDataPath+'/Most_likely_valuePID.txt','w+')
#f.write('Kp=%f;\n'%(value_mostlikely[0]))
#f.write('Ti=%f;\n'%(value_mostlikely[1]))
#f.write('Td=%f;\n'%(value_mostlikely[2]))
#f.close()
#fdp.savefig(path+'/HistogramPID.svg', format='svg', dpi=1000)
##
### Histogram for PI parameters
#fdp, ax = plt.subplots(1,2,figsize=(9,7))
#fdp.text(0.05, 0.5, 'Frequency', ha='center', va='center', rotation='vertical',fontsize=15)
#
#value_mostlikely=[]
#lowerLim=[]
#upperLim=[]
#label = ['Kp','Ti']
#for i in range(2):
#    ax1 = ax[i]
#    weights = np.ones_like(parameter0.iloc[:,i])/np.size(parameter0.iloc[:,i])
#    fr,binEdge = np.histogram(parameter0.iloc[:,i],bins=bins,weights = weights)
#    max_index = np.argmax(fr)
#    value_mostlikely.append(0.5*(binEdge[max_index]+binEdge[max_index+1]))
#    lowerLim.append(np.percentile(parameter0.iloc[:,i],5))
#    upperLim.append(np.percentile(parameter0.iloc[:,i],95))
#    index = []
#    for j in range(len(fr)):
#        mean=0.5*(binEdge[j]+binEdge[j+1])
#        if mean > lowerLim[i] and mean < upperLim[i]:
#            index.append(j)
#    probability=round(sum(fr[index]),3)
#    barlist = ax1.bar(binEdge[0:len(fr)],fr,width=binEdge[1]-binEdge[0],edgecolor='black',linewidth=1,color='0.75')
#    barlist[index[0]].set_label('5-95th Percentile')
#    for num in index:
#        barlist[num].set_color('mediumseagreen')
#        barlist[num].set_edgecolor('k')
#        barlist[num].set_linewidth('1')
#    
#    ax1.set_xlabel(label[i],fontsize=15)
#    ax1.legend(loc=0,fontsize=12)
#    if i == 0:
#        ax1.annotate('Kp='+str(round(value_mostlikely[i],3)),xy=(value_mostlikely[i],0.065),xytext=(-0.33,0.047),arrowprops=dict(arrowstyle='->'),fontsize=15,weight='demibold')
#    elif i == 1:
#        ax1.annotate('Ti='+str(round(value_mostlikely[i],3)),xy=(value_mostlikely[i]-0.2,0.085),xytext=(70,0.06),arrowprops=dict(arrowstyle='->'),fontsize=15,weight='demibold')
#f = open(RawDataPath+'/Most_likely_valuePI.txt','w+')
#f.write('Kp=%f;\n'%(value_mostlikely[0]))
#f.write('Ti=%f;\n'%(value_mostlikely[1]))
#f.close()
#fdp.savefig(path+'/HistogramPI.svg', format='svg', dpi=1000)


















