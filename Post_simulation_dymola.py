# -*- coding: utf-8 -*-
"""
Created on Fri Mar 16 16:12:02 2018

@author: Administrator
"""
import numpy as np
import sys
import os
from buildingspy.io.outputfile import Reader 
import pandas as pd
import shutil
from filter import modifiedEWMA as Mewma
from pso import pso
import matplotlib.pyplot as plt
import timeit
import winsound

   

def constraintManual(x,*args):
    t1 = x[0]
    t2 = x[1]
    return [t1-t2]   
def constraint(x,*args):
    t1 = x[1]
    t2 = x[2]
    return [t1-t2]


def transferFunctionPython(repoPath,baselineValue,step,startTime,period,timestep,TF,ManualK=True,steadyPeriod=None):
    #Using another modelica model "Transfer function" to calculate the simulated output 
    #then using PSO to identify the proper parameters
    #repoPath is the directory of the repository
    #baselineValue is the raw data from coupled FFD simulation
    #step is the size of the step signal
    #startTime is the starting time when the excitation signal is introduced
    #period is the time period during which the identification is performed
    #timestep is the time step size of sampling
    K = []
    a = []
    b = []
    T1 = []
    T2 = []
    D = []
    Fopt = []
    ## Calculate the initial value at the startTime
    IniVal = baselineValue[baselineValue.Time==startTime].iloc[:,1].values
    if ManualK:
        #Calculate K manually
        if steadyPeriod == None:
            K=(baselineValue[(baselineValue.Time==startTime+period)].iloc[:,1].values-baselineValue[(baselineValue.Time==startTime)].iloc[:,1].values)/step
            K = K[-1]
        else:
            valueBefore = baselineValue[(baselineValue.Time==startTime)].iloc[:,1].values
            valueAfter = baselineValue[(baselineValue.Time>=startTime+period-steadyPeriod) & (baselineValue.Time<=startTime+period)].iloc[:,1].mean()
            K = (valueAfter-valueBefore)/step
        #Define the other parameters in the objective function
        args = (repoPath,baselineValue,K,startTime,period,timestep,step,TF)
        #Define the lower and upper boundary for the parameters
        if TF == 'FirstOrder':
            lb = [0.01]
            ub = [500]
        elif TF == 'Oscillation':
            lb = [0.01,0]
            ub = [500,1]
        elif TF == 'SecondOrder_2':
            lb = [0,0]
            ub = [500,500]
        elif TF == 'SecondOrderPlusDelay':
            lb = [0,0,0]
            ub = [500,500,100]
        
        for i in range(1): # Perform n times of PSO to get a best result
            #xopt,fopt = pso(objectiveFunctionDymola,lb,ub,f_ieqcons=constraint,args = args, debug=True)
            if TF == 'FirstOrder':
                xopt,fopt = pso(objectiveFunctionPythonManual,lb,ub,args = args)
                T1.append(xopt[0])
                Fopt.append(fopt)
            elif TF == 'Oscillation':
                xopt,fopt = pso(objectiveFunctionPythonManual,lb,ub,args = args)
                a.append(xopt[0])
                b.append(xopt[1])
                Fopt.append(fopt)
            elif TF == 'SecondOrder_2':
                xopt,fopt = pso(objectiveFunctionPythonManual,lb,ub,f_ieqcons = constraintManual,args = args)
                T1.append(xopt[0])
                T2.append(xopt[1])
                Fopt.append(fopt)
            elif TF == 'SecondOrderPlusDelay':
                xopt,fopt = pso(objectiveFunctionPythonManual,lb,ub,f_ieqcons = constraintManual,args = args)
                T1.append(xopt[0])
                T2.append(xopt[1])
                D.append(xopt[2])
                Fopt.append(fopt)
        Kopt = K[-1]
        if TF == 'FirstOrder':
            T1opt = T1[Fopt.index(min(Fopt))]
            fopt = min(Fopt)
            return Kopt, T1opt, fopt, IniVal
        elif TF == 'Oscillation':
            aopt = a[Fopt.index(min(Fopt))]
            bopt= b[Fopt.index(min(Fopt))]
            fopt = min(Fopt)
            return Kopt, aopt, bopt, fopt, IniVal
        elif TF == 'SecondOrder_2':
            T1opt = T1[Fopt.index(min(Fopt))]
            T2opt= T2[Fopt.index(min(Fopt))]
            fopt = min(Fopt)
            return Kopt, T1opt, T2opt, fopt, IniVal
        elif TF == 'SecondOrderPlusDelay':
            T1opt = T1[Fopt.index(min(Fopt))]
            T2opt= T2[Fopt.index(min(Fopt))]
            Dopt = D[Fopt.index(min(Fopt))]
            fopt = min(Fopt)
            return Kopt,T1opt,T2opt,Dopt,fopt, IniVal
    else:
        #Define the other parameters in the objective function
        args = (repoPath,baselineValue, startTime,period,timestep,step,TF)
        #Define the lower and upper boundary for the parameters
        if TF == 'FirstOrder':
            lb = [-10,0.01]
            ub = [-0.01,500]
        elif TF == 'Oscillation':
            lb = [-10,0.01,0]
            ub = [-0.01,500,1]
        elif TF == 'SecondOrder_2':
            lb = [-10,0.01,0.01]
            ub = [-0.01,500,300]
        elif TF == 'SecondOrderPlusDelay':
            lb = [-10,0.01,0.01,0]
            ub = [-0.01,500,300,100]
        
        for i in range(3): # Perform n times of PSO to get a best result
            #xopt,fopt = pso(objectiveFunctionDymola,lb,ub,f_ieqcons=constraint,args = args, debug=True)
            if TF == 'FirstOrder':
                xopt,fopt = pso(objectiveFunctionPython,lb,ub,args = args)
                K.append(xopt[0])
                T1.append(xopt[1])
                Fopt.append(fopt)
            elif TF == 'Oscillation':
                xopt,fopt = pso(objectiveFunctionPython,lb,ub,args = args)
                K.append(xopt[0])
                a.append(xopt[1])
                b.append(xopt[2])
                Fopt.append(fopt)
            elif TF == 'SecondOrder_2':
                xopt,fopt = pso(objectiveFunctionPython,lb,ub,f_ieqcons = constraint,args = args)
                K.append(xopt[0])
                T1.append(xopt[1])
                T2.append(xopt[2])
                Fopt.append(fopt)
            elif TF == 'SecondOrderPlusDelay':
                xopt,fopt = pso(objectiveFunctionPython,lb,ub,f_ieqcons = constraint,args = args)
                K.append(xopt[0])
                T1.append(xopt[1])
                T2.append(xopt[2])
                D.append(xopt[3])
                Fopt.append(fopt)
        Kopt = K[Fopt.index(min(Fopt))]    
        if TF == 'FirstOrder':
            T1opt = T1[Fopt.index(min(Fopt))]
            fopt = min(Fopt)
            return Kopt, T1opt, fopt, IniVal
        elif TF == 'Oscillation':
            aopt = a[Fopt.index(min(Fopt))]
            bopt= b[Fopt.index(min(Fopt))]
            fopt = min(Fopt)
            return Kopt, aopt, bopt, fopt, IniVal
        elif TF == 'SecondOrder_2':
            T1opt = T1[Fopt.index(min(Fopt))]
            T2opt= T2[Fopt.index(min(Fopt))]
            fopt = min(Fopt)
            return Kopt, T1opt, T2opt, fopt, IniVal
        elif TF == 'SecondOrderPlusDelay':
            T1opt = T1[Fopt.index(min(Fopt))]
            T2opt= T2[Fopt.index(min(Fopt))]
            Dopt = D[Fopt.index(min(Fopt))]
            fopt = min(Fopt)
            return Kopt,T1opt,T2opt,Dopt,fopt ,IniVal


def objectiveFunctionPythonManual(x,*args):
    
    repoPath, meaDF,K,startTime, period, timestep,step, TF = args
    
    ## Calculate the initial value at the startTime
    IniVal = meaDF[meaDF.Time==startTime].iloc[:,1].values
    # Create time series
    realtime = np.arange(startTime,startTime+period+timestep,timestep)
    timetag = realtime-startTime
    # Calculate corresponding output of transfer function
    # vectorize transfer function
    if TF == 'FirstOrder':
        T1 = x
        Vector = np.vectorize(FirstOrder,otypes=[np.float])
        output = Vector(K, T1, step, timetag)+IniVal
    elif TF == 'Oscillation':
        a, b  = x
        Vector = np.vectorize(Oscillation,otypes=[np.float])
        output = Vector(K, a, b, step, timetag)+IniVal
    elif TF == 'SecondOrder_2':
        T1, T2  = x
        Vector = np.vectorize(SecondOrder_2,otypes=[np.float])
        output = Vector(K, T1, T2, step, timetag)+IniVal
    elif TF == 'SecondOrderPlusDelay':
        T1, T2, D = x
        V2ndOrderPlusDelay = np.vectorize(SecondOrderPlusDelay,otypes=[np.float])
        output = V2ndOrderPlusDelay(K, T1, T2, D, step, timetag)+IniVal
    DF = {'Time':realtime,'simTemp':output}
    resultDF = pd.DataFrame(data = DF)

    #simDF and meaDF are both type of series
    #Slice the data after the startime and within the transition period
    simDF = resultDF[(resultDF.Time>=startTime)&(resultDF.Time<=startTime+period)]
    meaDF = meaDF[(meaDF.Time>=startTime)&(meaDF.Time<=startTime+period)]
    #Drop dupilicates
    simDF = simDF.drop_duplicates("Time")
    meaDF = meaDF.drop_duplicates("Time")
    if np.size(simDF,axis=0) != np.size(meaDF,axis=0):
        sys.exit("The dimensions of simDF and MeaDF don't match with each other!")
   
    #Calculate the error between the simulated value using transfer function and the coupled FFD simulation
    #Both the error of the points and the error of slope between two adjacent points are taken into account 
    absError = np.sum(np.square(simDF.iloc[:,1].values-meaDF.iloc[:,1].values))
#    slope_sim = np.zeros([np.size(simDF)-1,1])
#    slope_mea = np.zeros([np.size(meaDF)-1,1])
#    slope_sim = (simDF.iloc[1:,1].values-simDF.iloc[:-1,1].values)/timestep
#    slope_mea = (meaDF.iloc[1:,1].values-meaDF.iloc[:-1,1].values)/timestep
#    slopeError = np.sum(np.abs(slope_sim-slope_mea))
#    objVal = np.log(absError)+np.log(slopeError)
#    objVal = absError*slopeError
#    objVal = np.log(absError)
    return absError

def objectiveFunctionPython(x,*args):
    
    repoPath, meaDF,startTime, period, timestep,step, TF = args
    
    ## Calculate the initial value at the startTime
    IniVal = meaDF[meaDF.Time==startTime].iloc[:,1].values
    # Create time series
    realtime = np.arange(startTime,startTime+period+timestep,timestep)
    timetag = realtime-startTime
    # Calculate corresponding output of transfer function
    # vectorize transfer function
    if TF == 'FirstOrder':
        K, T1 = x
        Vector = np.vectorize(FirstOrder,otypes=[np.float])
        output = Vector(K, T1, step, timetag)+IniVal
    elif TF == 'Oscillation':
        K, a, b = x
        Vector = np.vectorize(Oscillation,otypes=[np.float])
        output = Vector(K, a, b, step, timetag)+IniVal
    elif TF == 'SecondOrder_2':
        K, T1, T2 = x
        Vector = np.vectorize(SecondOrder_2,otypes=[np.float])
        output = Vector(K, T1, T2, step, timetag)+IniVal
    elif TF == 'SecondOrderPlusDelay':
        K, T1, T2, D = x
        V2ndOrderPlusDelay = np.vectorize(SecondOrderPlusDelay,otypes=[np.float])
        output = V2ndOrderPlusDelay(K, T1, T2, D, step, timetag)+IniVal
    DF = {'Time':realtime,'simTemp':output}
    resultDF = pd.DataFrame(data = DF)

    #simDF and meaDF are both type of series
    #Slice the data after the startime and within the transition period
    simDF = resultDF[(resultDF.Time>=startTime)&(resultDF.Time<=startTime+period)]
    meaDF = meaDF[(meaDF.Time>=startTime)&(meaDF.Time<=startTime+period)]
    #Drop dupilicates
    simDF = simDF.drop_duplicates("Time")
    meaDF = meaDF.drop_duplicates("Time")
    if np.size(simDF,axis=0) != np.size(meaDF,axis=0):
        sys.exit("The dimensions of simDF and MeaDF don't match with each other!")
   
    #Calculate the error between the simulated value using transfer function and the coupled FFD simulation
    #Both the error of the points and the error of slope between two adjacent points are taken into account 
    absError = np.sum(np.square(simDF.iloc[:,1].values-meaDF.iloc[:,1].values))
#    slope_sim = np.zeros([np.size(simDF)-1,1])
#    slope_mea = np.zeros([np.size(meaDF)-1,1])
#    slope_sim = (simDF.iloc[1:,1].values-simDF.iloc[:-1,1].values)/timestep
#    slope_mea = (meaDF.iloc[1:,1].values-meaDF.iloc[:-1,1].values)/timestep
#    slopeError = np.sum(np.abs(slope_sim-slope_mea))
#    objVal = np.log(absError)+np.log(slopeError)
#    objVal = absError*slopeError
#    objVal = np.log(absError)
    return absError

def FirstOrder(K,T1,step,time):
    output = step*K*(1-np.exp(-time/T1))
    return output

def Oscillation(K,T,eta,step,t):
#    output = step*K*(1-np.exp(-b*t/2*a)*(np.cosh((t*np.sqrt(np.square(b)/4-a))/a)+b*np.sinh((t*np.sqrt(np.square(b)/4-a))/a)/(2*np.sqrt(np.square(b)/4-a))))
    output = step*K*(1 - np.exp(-(eta*t)/T)*(np.cosh((t*(eta**2 - 1)**(1/2))/T) + (eta*np.sinh((t*(eta**2 - 1)**(1/2))/T))/((eta**2- 1)**(1/2))))

    return output

def SecondOrder_2(K,T1,T2,step,t):
    if T2-T1 == 0 or T1 == 0 or T2== 0:
        output = 0
    else:
        output = step*K*(1-np.exp(-t/T1)*T1/(T1-T2)+np.exp(-t/T2)*T2/(T1-T2))
    return output

   
def SecondOrderPlusDelay(K,T1,T2,D,step,time):
    if time <= D:
        output = 0
    else:
        if T2-T1 == 0 or T1 == 0 or T2 == 0:
            output = 0
        else:
            output = step*K*(1+T1*T2/(T2-T1)*(np.exp(-(time-D)/T1)/T2-np.exp(-(time-D)/T2)/T1))
        
    return output
       
def comparisonPython(step,meaDF,startTime,period,timestep,TF,args=()):
    ## Calculate the initial value at the startTime
    IniVal = meaDF[meaDF.Time==startTime].iloc[:,1].values
    # Create time series
    realtime = np.arange(startTime,startTime+period+timestep,timestep)
    timetag = realtime-startTime
    # Calculate corresponding output of transfer function
    # vectorize transfer function
    if TF == 'FirstOrder':
        K, T1 = args
        Vector = np.vectorize(FirstOrder,otypes=[np.float])
        output = Vector(K, T1, step, timetag)+IniVal
    elif TF == 'Oscillation':
        K, a, b = args
        Vector = np.vectorize(Oscillation,otypes=[np.float])
        output = Vector(K, a, b, step, timetag)+IniVal
    elif TF == 'SecondOrder_2':
        K, T1, T2 = args
        Vector = np.vectorize(SecondOrder_2,otypes=[np.float])
        output = Vector(K, T1, T2, step, timetag)+IniVal
    elif TF == 'SecondOrderPlusDelay':
        K, T1, T2, D = args
        V2ndOrderPlusDelay = np.vectorize(SecondOrderPlusDelay,otypes=[np.float])
        output = V2ndOrderPlusDelay(K, T1, T2, D, step, timetag)+IniVal
   
    DF = {'Time':realtime,'simTemp':output}
    resultDF = pd.DataFrame(data = DF)
    
    #simDF and meaDF are both type of series
    #Slice the data after the startime and within the transition period
    simDF = resultDF[(resultDF.Time>=startTime)&(resultDF.Time<=startTime+period)]
    meaDF = meaDF[(meaDF.Time>=startTime)&(meaDF.Time<=startTime+period)]
    #Drop dupilicates
    simDF = simDF.drop_duplicates("Time")
    meaDF = meaDF.drop_duplicates("Time")
    if np.size(simDF,axis=0) != np.size(meaDF,axis=0):
        sys.exit("The dimensions of simDF and MeaDF don't match with each other!")
    
     # Plot simDF and meaDF
    fig,ax = plt.subplots(1,1,figsize=(6,4))
    ax.plot(simDF["Time"].values,simDF.iloc[:,1].values-273.15,'b',label = "Transfer Function #2",linewidth=1.2)
    ax.plot(meaDF["Time"].values,meaDF.iloc[:,1].values-273.15,'r--',label = "Modelica Output",linewidth=1.2)
    
    ax.legend(loc=0,fontsize=12)
    ax.set_xlabel('Time/ s',fontsize=15)
    ax.set_ylabel(meaDF.columns[1]+'/$^\circ$C',fontsize=15)      
    fig.savefig(repoPath+'/Transfer Function Identification_dymola.svg',format='svg',dpi=1000)      
    plt.show()



# Main script
if __name__ == "__main__":
    Start = timeit.default_timer()
    
    repoPath = os.getcwd()
    PackagePath = repoPath+'/ModelicaModels'
    ResultPath = repoPath+'/Results_dymola'
    
    ScenarioNum = 1
    Finished = 0
    #Options for start over or not
    startOver = raw_input('Are you sure you want to start over? Answer with y/n____________')
    if startOver == 'y':
        #Purge old results
        if os.path.exists(ResultPath+'/Summary'):
            shutil.rmtree(ResultPath+'/Summary')
            os.mkdir(ResultPath+'/Summary')
        else:
            os.mkdir(ResultPath+'/Summary')
    elif startOver == 'n':
        Finished = input('How many cases have been finished? Answer with Integers_____________')
        if type(Finished) != int:
            sys.exit('I said answer with integer!!!')  
    
    ## Read all mat results of different scenarios
    #Loop in scnearios
    for i in np.arange(Finished,Finished+ScenarioNum,1):
        ###########################################################
        #This section is to generate all the results of the local temperatures
        ResultFile = os.path.join(ResultPath,"Scenario-"+str(i+1),
                                  "dsres_1.mat")
        #Read temperature data 
        r = Reader(ResultFile,"dymola")
        (time,temp) = r.values('FinalValue.y')
        LocTemp = np.zeros([np.size(time,axis=0),17])
        for j in range(17):               #loop in 16 sensors and Avetemp
            #Creat dataframe for this scenario
            #Read time and temperature data
            ResultFile = os.path.join(ResultPath,"Scenario-"+str(i+1),
                                      "dsres_"+str(j)+".mat")
            #Read temperature data 
            r = Reader(ResultFile,"dymola")
            (time,temp) = r.values('FinalValue.y')
            LocTemp[:,j] = temp
        #Concat all the data into one dataframe
        timeDF = pd.DataFrame(time,columns=['Time'])
        LocTempDF = pd.DataFrame(LocTemp,columns=['AveTemp','LocTemp1','LocTemp2','LocTemp3','LocTemp4',
                                                  'LocTemp5','LocTemp6','LocTemp7','LocTemp8',
                                                  'LocTemp9','LocTemp10','LocTemp11','LocTemp12',
                                                  'LocTemp13','LocTemp14','LocTemp15','LocTemp16'])
        Data = pd.concat([timeDF, LocTempDF],axis=1)
        
        UniqueData = Data.drop_duplicates('Time')
        UniqueData.index = range(np.size(UniqueData,axis=0))
        UniqueData.to_csv(ResultPath+'/Summary/Dymola Results_Scenario-'+str(i+1)+'.csv')
               
        ##Identify the parameters of transfer function
        # Create a dataframe to restore the transfer fucntion parameters
#        K = np.zeros([1,16])
#        T1 = np.zeros([1,16])
#        T2 = np.zeros([1,16])
#        D = np.zeros([1,16])
#        IniVal = np.zeros([1,16])
#        K_DF = pd.DataFrame(K,columns=list(UniqueData.iloc[:,1:17]))
#        T1_DF = pd.DataFrame(T1,columns=list(UniqueData.iloc[:,1:17]))
#        T2_DF = pd.DataFrame(T2,columns=list(UniqueData.iloc[:,1:17]))
#        D_DF = pd.DataFrame(D,columns=list(UniqueData.iloc[:,1:17]))
#        IniVal_DF = pd.DataFrame(IniVal,columns=list(UniqueData.iloc[:,1:17]))
#        TFparameters_DF = pd.concat([K_DF,T1_DF,T2_DF,D_DF,IniVal_DF],axis=0)
#        TFparameters_DF.index = ['K','T1','T2','D','IniVal']

#         for j in range(16):                   #the number of loop iteration is the number of sensors 16 in total
#            # Identify K, T1, T2, D
#             TF = 'SecondOrderPlusDelay'
#             [k,t1,t2,d, fopt,iniVal] = transferFunctionPython(repoPath,UniqueData.iloc[:,[0,j+1]],0.6,100,1900,5,TF,ManualK=True,steadyPeriod=50)
#             # Save P I into the dataframe
#             TFparameters_DF.iloc[:,j] = [k,t1,t2,d,iniVal[0]]
#             args = (k,t1,t2,d)
#        #            comparisonPython(0.6,UniqueData.iloc[:,[0,j+1]],100,900,5,TF,args=args)
#            # Save parameters
#         TFparameters_DF.to_csv(ResultPath+'/Summary/Transfer Function_Scenario-'+str(i+1)+'.csv')
#        
#    Time = timeit.default_timer()-Start
        
        
        ###############################################################
        #This section is to generate the average temperature only
#        Data = pd.read_csv(os.path.join(repoPath,'Results_dymola','Summary','Dymola Results_Scenario-'+str(i+1)+'.csv')).iloc[:,1:]
#        ResultFile = os.path.join(ResultPath,"Scenario-"+str(i+1),
#                                  "dsres_0.mat")
#        r = Reader(ResultFile,"dymola")
#        (time,temp) = r.values('FinalValue.y')
#        timeDF = pd.DataFrame(time,columns=['Time'])
#        AveTempDF = pd.DataFrame(temp,columns=['AveTemp'])
#        AveData= pd.concat([timeDF,AveTempDF],axis=1)                                        
#        UniqueAveData = AveData.drop_duplicates('Time')
##        Data['AveTemp']=UniqueAveData['AveTemp']   
#        Data['AveTemp'].values = UniqueAveData['AveTemp'].values
##        Data = pd.concat([Data,UniqueAveData['AveTemp']],axis=1)                             
#        Data.to_csv(ResultPath+'/Summary/Dymola Results_Scenario-'+str(i+1)+'.csv')                                     
                                                  
        TFparameters_DF = pd.read_csv(os.path.join(repoPath,'Results_dymola','Summary','Transfer Function_Scenario-'+str(i+1)+'.csv')).iloc[:,1:]
        
        for j in np.arange(1,2,1):                 #the number of loop iteration is the number of sensors 16 in total
            # Identify K, T1, T2, D
            TF = 'SecondOrderPlusDelay'
            [k,t1,t2,d, fopt,iniVal] = transferFunctionPython(repoPath,UniqueData.iloc[:,[0,j+1]],0.6,100,1900,5,TF,ManualK=True,steadyPeriod=50)
            # Save P I into the dataframe
            TFparameters_DF['AveTemp'] = [k,t1,t2,d,iniVal[0]]
            args = (k,t1,t2,d)
            comparisonPython(0.6,UniqueData.iloc[:,[0,j+1]],100,1900,5,TF,args=args)
        # Save parameters
        TFparameters_DF.to_csv(ResultPath+'/Summary/Transfer Function_Scenario-'+str(i+1)+'.csv')

    Time = timeit.default_timer()-Start




duration = 1000
freq = 900
winsound.Beep(freq,duration)